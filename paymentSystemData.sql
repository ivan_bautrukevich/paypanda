use paypanda_system;

INSERT INTO role(role) VALUES 
('admin'),
('user');

INSERT INTO user(first_name, last_name, email, login, password, role_id) VALUES 
('Иван', 'Иванов', 'bautruk@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '21232f297a57a5a743894a0e4a801fc3', 1),
('Петя', 'Петров', 'user@gmail.com', 'ee11cbb19052e40b07aac0ca060c23ee', 'ee11cbb19052e40b07aac0ca060c23ee', 2);

INSERT INTO currency(code_currency, currency) VALUES 
(974, 'BYR'),
(840, 'USD'),
(978, 'EUR'),
(643, 'RUB');

INSERT INTO status(status) VALUES 
('Разблокирован'),
('Заблокирован');

INSERT INTO card_type(card_type) VALUES 
('Дебетовая'),
('Кредитовая');

INSERT INTO payment_system_type(payment_system_type) VALUES 
('VISA'),
('Белкарт'),
('MasterCard');

INSERT INTO card(card_number, user_id, card_type_id, payment_system_type_id, cvv, first_name_holder, last_name_holder, code_currency) VALUES 
(1234123412340001, 2, 1, 1, 111, 'Иван', 'Ивановн', 974),
(1234123412340002, 2, 1, 1, 111, 'Петр', 'Петров', 840);

INSERT INTO payment_account(card_id, payment_account_number, balance, status_id) VALUES 
(1, 12345678900001, 1000.2, 1),
(2, 12345678900002, 500, 1);

INSERT INTO payment_type(payment_type) VALUES 
('бюджет'),
('налог'),
('кредит');

INSERT INTO payment(payment_account_id, date, external_account, sum, description, payment_type_id) VALUES 
(1, "2015-01-01", 1234567890123, 100, 'от Иванова', 1),
(1, "2015-01-01", 1234567890123, 200, 'от Иванова', 1),
(1, "2015-01-01", 1234567890123, 300, 'от Иванова', 1),
(1, "2015-01-01", 1234567890123, 400, 'от Иванова', 1),
(2, "2014-03-05", 1234567890123, 100.2, 'от Петрова', 3),
(2, "2014-03-05", 1234567890123, 500.2, 'от Петрова', 3),
(2, "2014-03-05", 1234567890123, 400.2, 'от Петрова', 3),
(2, "2014-03-05", 1234567890123, 300.2, 'от Петрова', 3),
(2, "2014-03-05", 1234567890123, 100.2, 'от Петрова', 3);