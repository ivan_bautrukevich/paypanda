<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 13.01.2016
  Time: 19:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="pagecontent" prefix="page.admin.">
    <head>
        <link href="/image/favicon.ico" rel="shortcut icon" type="image/x-icon">
        <title><fmt:message key="title"/></title>
    </head>
    <body>
    <div class="central">
        <div class="head">
            <%@ include file="../include/header.jsp" %>
        </div>
        <div class="menuUser">
            <%@ include file="../include/menuAdmin.jsp" %>
        </div>
        <div class="main">
            <c:choose>
                <c:when test="${empty message}">
                    <fmt:message key="hello"/>, ${user.firstName} ${user.lastName}!
                </c:when>
                <c:when test="${not empty message}">
                    ${user.firstName} ${user.lastName}, ${message}
                </c:when>
            </c:choose>
        </div>
        <%@ include file="../include/footer.jsp" %>
    </div>
    </body>
    </html>
</fmt:bundle>