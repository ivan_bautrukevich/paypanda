<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 22.01.2016
  Time: 16:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="pagecontent" prefix="page.addMoney.">
    <head>
        <link href="/image/favicon.ico" rel="shortcut icon" type="image/x-icon">
        <title><fmt:message key="title"/></title>
    </head>
    <body>
    <div class="central">
        <div class="head">
            <%@ include file="../include/header.jsp" %>
        </div>
        <div class="menuUser">
            <%@ include file="../include/menuUser.jsp" %>
        </div>
        <div class="main">
            <c:choose>
            <c:when test="${accounts.size() > 0}">
            <form name="loginForm" method="POST" action="controller">
                <input type="hidden" name="command" value="add_money"/>
                <fmt:message key="ad"/>
                <table align="center">
                    <tr>
                        <td>
                            <fmt:message key="selectAccount"/>
                        </td>
                        <td>
                            <select name="selectAccount">
                                <c:forEach var="account" items="${accounts}">
                                    <option selected="select">${account.number}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fmt:message key="sum"/>
                        </td>
                        <td>
                            <input required type="text" name="sum" value="" pattern="^((\d){1,10}[.]*){1}\d{0,2}$"
                                   title="<fmt:message key="sum.regex"/>"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" , align="center">
                            <input class="secondButton" type="submit" value="<fmt:message key="submit"/>"/>
                        </td>
                    </tr>
                </table>
                </c:when>
                <c:otherwise>
                    <fmt:message key="message"/>
                </c:otherwise>
                </c:choose>
            </form>
        </div>
        <%@ include file="../include/footer.jsp" %>
    </div>
    </body>
    </html>
</fmt:bundle>
