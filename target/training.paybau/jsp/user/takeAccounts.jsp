<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 22.01.2016
  Time: 14:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="pagecontent" prefix="page.takeAccounts.">
    <head>
        <link href="/image/favicon.ico" rel="shortcut icon" type="image/x-icon">
        <title><fmt:message key="title"/></title>
    </head>
    <body>
    <div class="central">
        <div class="head">
            <%@ include file="../include/header.jsp" %>
        </div>
        <div class="menuUser">
            <%@ include file="../include/menuUser.jsp" %>
        </div>
        <div class="main">
            <c:choose>
                <c:when test="${accounts.size() > 0}">
                    <table class="tablePanda">
                        <tr>
                            <th><fmt:message key="accountNumber"/></th>
                            <th><fmt:message key="balance"/></th>
                            <th><fmt:message key="cardNumber"/></th>
                            <th><fmt:message key="status"/></th>
                            <th><fmt:message key="currency"/></th>
                        </tr>
                        <c:forEach var="account" items="${accounts}">
                            <tr>
                                <td>${account.number}</td>
                                <td>${account.balance}</td>
                                <td>${account.card.number}</td>
                                <td>${account.paymentAccountStatus.status}</td>
                                <td>${account.card.currencyType.name}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </c:when>
                <c:otherwise>
                    <fmt:message key="message"/>
                </c:otherwise>
            </c:choose>
        </div>
        <%@ include file="../include/footer.jsp" %>
    </div>
    </body>
    </html>
</fmt:bundle>
