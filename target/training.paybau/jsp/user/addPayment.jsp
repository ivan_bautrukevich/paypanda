<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 17.01.2016
  Time: 20:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="pagecontent" prefix="page.addPayment.">
    <head>
        <link href="/image/favicon.ico" rel="shortcut icon" type="image/x-icon">
        <title><fmt:message key="title"/></title>
    </head>
    <body>
    <div class="central">
        <div class="head">
            <%@ include file="../include/header.jsp" %>
        </div>
        <div class="menuUser">
            <%@ include file="../include/menuUser.jsp" %>
        </div>
        <div class="main">
            <fmt:message key="ad"/>
            <form name="loginForm" method="POST" action="controller">
                <input type="hidden" name="command" value="add_payment"/>
                <table align="center">
                    <tr>
                        <td>
                            <fmt:message key="selectCard"/>
                        </td>
                        <td>
                                ${card.number}
                            <input type="hidden" name="selectCard" value="${card.number}"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fmt:message key="externalAccount"/>
                        </td>
                        <td>
                            <input required type="text" name="externalAccount" value="" pattern="^\d{13}$"
                                   title="<fmt:message key="externalAccount.regex"/>"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fmt:message key="sum"/>
                        </td>
                        <td>
                            <input required type="text" name="sum" value="" pattern="^((\d){1,10}[.]*){1}\d{0,2}$"
                                   title="<fmt:message key="sum.regex"/>"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fmt:message key="description"/>
                        </td>
                        <td>
                            <input required type="text" name="description" value="" pattern="[\w]{1,20}"
                                   title="<fmt:message key="description.regex"/>"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fmt:message key="selectType"/>
                        </td>
                        <td>
                            <select name="selectType">
                                <c:forEach var="type" items="${types}">
                                    <option selected="select">${type.type}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fmt:message key="cvv"/>
                        </td>
                        <td>
                            <input required type="text" name="cvv" value="" pattern="^[0-9]{3}$"
                                   title="<fmt:message key="cvv.regex"/>"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <input class="secondButton" type="submit" value="<fmt:message key="submit"/>"/>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        <%@ include file="../include/footer.jsp" %>
    </div>
    </body>
    </html>
</fmt:bundle>
