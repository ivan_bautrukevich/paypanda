<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 31.01.2016
  Time: 13:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="pagecontent" prefix="page.blockAccount.">
    <head>
        <link href="/image/favicon.ico" rel="shortcut icon" type="image/x-icon">
        <title><fmt:message key="title"/></title>
    </head>
    <body>
    <div class="central">
        <div class="head">
            <%@ include file="../include/header.jsp" %>
        </div>
        <div class="menuUser">
            <%@ include file="../include/menuUser.jsp" %>
        </div>
        <div class="main">
            <c:choose>
                <c:when test="${accounts.size() > 0}">
                    <fmt:message key="ad"/>
                    <form name="loginForm" method="POST" action="controller">
                        <input type="hidden" name="command" value="block_account"/>
                        <table align="center">
                            <tr>
                                <td>
                                    <fmt:message key="selectAccount"/>
                                </td>
                                <td>
                                    <select name="selectAccount">
                                        <c:forEach var="account" items="${accounts}">
                                            <option selected="select">${account.number}</option>
                                        </c:forEach>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">
                                    <input class="secondButton" type="submit" value="<fmt:message key="submit"/>"/>
                                </td>
                            </tr>
                        </table>
                    </form>
                </c:when>
                <c:otherwise>
                    <fmt:message key="message"/>
                </c:otherwise>
            </c:choose>

        </div>
        <%@ include file="../include/footer.jsp" %>
    </div>
    </body>
    </html>
</fmt:bundle>
