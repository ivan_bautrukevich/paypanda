<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 19.01.2016
  Time: 13:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="pagecontent" prefix="page.takeCards.">
    <head>
        <link href="/image/favicon.ico" rel="shortcut icon" type="image/x-icon">
        <title><fmt:message key="title"/></title>
    </head>
    <body>
    <div class="central">
        <div class="head">
            <%@ include file="../include/header.jsp" %>
        </div>
        <div class="menuUser">
            <%@ include file="../include/menuUser.jsp" %>
        </div>
        <div class="main">
            <c:choose>
                <c:when test="${cards.size() > 0}">
                    <table class="tablePanda">
                        <tr>
                            <th><fmt:message key="cardNumber"/></th>
                            <th><fmt:message key="firstName"/></th>
                            <th><fmt:message key="lastName"/></th>
                            <th><fmt:message key="type"/></th>
                            <th><fmt:message key="paymentType"/></th>
                            <th><fmt:message key="currency"/></th>
                        </tr>
                        <c:forEach var="card" items="${cards}">
                            <tr>
                                <td>${card.number}</td>
                                <td>${card.firstNameHolder}</td>
                                <td>${card.lastNameHolder}</td>
                                <td>${card.cardType.type}</td>
                                <td>${card.paymentSystemType.type}</td>
                                <td>${card.currencyType.name}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </c:when>
                <c:otherwise>
                    <fmt:message key="message"/>
                </c:otherwise>
            </c:choose>
        </div>
        <%@ include file="../include/footer.jsp" %>
    </div>
    </body>
    </html>
</fmt:bundle>
