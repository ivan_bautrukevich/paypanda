<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 22.01.2016
  Time: 11:13
  To change this template use File | Settings | File Templates.
--%>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="pagecontent" prefix="page.language.">
    <div class="white">
        <a class="secondButton" href="controller?command=logout"><fmt:message key="logout"/></a>
    </div>
</fmt:bundle>
