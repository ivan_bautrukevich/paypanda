<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 01.02.2016
  Time: 21:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>

<fmt:bundle basename="pagecontent" prefix="page.user.">
    <footer>
        <div class="tag">
            <ctg:info-time/>
        </div>
        <div class="copyright">
            <fmt:message key="footer"/>
        </div>
    </footer>
</fmt:bundle>