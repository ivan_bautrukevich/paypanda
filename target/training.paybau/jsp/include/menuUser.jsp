<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 16.01.2016
  Time: 18:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="pagecontent" prefix="page.menuUser.">
    <head>
        <link rel="stylesheet" type="text/css" href="../css/style.css">
    </head>
    <body>
    <div>
        <details>
            <summary><fmt:message key="payments"/></summary>
            <h4><a href="controller?command=take_payments"><fmt:message
                    key="payments.takePayments"/></a></h4>
            <h4><a href="controller?command=ask_form_payment_first"><fmt:message
                    key="payments.addPayment"/></a></h4>
        </details>
        <br><br>
        <details>
            <summary><fmt:message key="accounts"/></summary>
            <h4><a href="controller?command=take_accounts"><fmt:message
                    key="accounts.takeAccounts"/></a></h4>
            <h4><a href="controller?command=ask_form_money"><fmt:message
                    key="accounts.addMoney"/></a></h4>
            <h4><a href="controller?command=ask_form_account"><fmt:message
                    key="accounts.addAccount"/></a></h4>
            <h4><a href="controller?command=ask_form_block"><fmt:message
                    key="accounts.blockAccount"/></a></h4>
        </details>
        <br><br>
        <details>
            <summary><fmt:message key="cards"/></summary>
            <h4><a href="controller?command=take_cards"><fmt:message
                    key="cards.takeCards"/></a></h4>
            <h4><a href="controller?command=ask_form_card"><fmt:message
                    key="cards.addCards"/></a>
            </h4>
        </details>
    </div>
    </body>
    </html>
</fmt:bundle>
