<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 13.01.2016
  Time: 12:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="pagecontent" prefix="page.menuAdmin.">
    <head>
        <link rel="stylesheet" type="text/css" href="../css/style.css">
    </head>
    <body>
    <div>
        <table>
            <tr>
                <td>
                    <details>
                        <summary><fmt:message key="payments"/></summary>
                        <h4><a href="controller?command=take_payments_day"><fmt:message key="paymentsPeriod"/></a></h4>
                    </details>
                    <br><br>
                    <details>
                        <summary><fmt:message key="paymentAccount"/></summary>
                        <h4><a href="controller?command=ask_form_unblock"><fmt:message key="paymentAccountUnblock"/></a>
                        </h4>
                    </details>
                    <br><br>
                    <details>
                        <summary><fmt:message key="users"/></summary>
                        <h4><a href="controller?command=take_users"><fmt:message key="takeUsers"/></a></h4>
                        <h4><a href="controller?command=ask_form_user"><fmt:message key="addUser"/></a></h4>
                    </details>
                </td>
            </tr>
        </table>
    </div>
    </body>
    </html>
</fmt:bundle>