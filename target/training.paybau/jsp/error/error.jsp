<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 12.01.2016
  Time: 19:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page isErrorPage="true" contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="pagecontent" prefix="page.error.">
    <head>
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <link href="/image/favicon.ico" rel="shortcut icon" type="image/x-icon">
        <title><fmt:message key="title"/></title>
    </head>
    <body>
    <div class="central">
        <div class="head">
            <div class="imagePanda">
                <a href="controller?command=login"><img src="/image/panda.JPG" height="100" , width="154"></a>
            </div>
            <div>
                <h1>PayPanda</h1>
            </div>
            <div class="headerTable">
                <table>
                    <tr>
                        <td align="center">
                            <%@ include file="../include/logout.jsp" %>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="mainError">
            <input type="hidden" name="command" value="add_payment"/>
            <c:choose>
                <c:when test="${not empty message}">
                    <fmt:message key="message"/> ${message}
                </c:when>
                <c:otherwise>
                    <fmt:message key="request"/> ${pageContext.errorData.requestURI} <fmt:message key="fail"/>
                    <br/>
                    <fmt:message key="servlet"/> ${pageContext.errorData.servletName}
                    <br/>
                    <fmt:message key="status"/> ${pageContext.errorData.statusCode}
                    <br/>
                    <fmt:message key="exception"/> ${pageContext.errorData.throwable}
                    <br/>
                    <fmt:message key="message.exception"/> ${pageContext.exception.message}
                    <br/>
                </c:otherwise>
            </c:choose>
        </div>
        <%@ include file="../include/footer.jsp" %>
    </div>
    </body>
    </html>
</fmt:bundle>