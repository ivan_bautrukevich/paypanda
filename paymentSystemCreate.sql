create DATABASE if not exists paypanda_system;

use paypanda_system;

CREATE TABLE IF NOT EXISTS role (
  role_id int(10) NOT NULL AUTO_INCREMENT,
  role varchar(64) not null,
  PRIMARY KEY (role_id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS user (
  user_id int(10) NOT NULL AUTO_INCREMENT,
  first_name varchar(64) not null,
  last_name varchar(64) not null,
  email varchar(64) not null,
  login varchar(64) unique not null,
  password varchar(64) not null,
  role_id int(10) not null,
  PRIMARY KEY (user_id),
  foreign key(role_id) references role(role_id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS currency (
  code_currency int(3) not null,
  currency varchar(64) not null,
  PRIMARY KEY (code_currency)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS status (
  status_id int(10) NOT NULL AUTO_INCREMENT,
  status varchar(64) not null,
  PRIMARY KEY (status_id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS payment_system_type (
  payment_system_type_id int(10) NOT NULL AUTO_INCREMENT,
  payment_system_type varchar(64) not null,
  PRIMARY KEY (payment_system_type_id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS card_type (
  card_type_id int(10) NOT NULL AUTO_INCREMENT,
  card_type varchar(64) not null,
  PRIMARY KEY (card_type_id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS card (
  card_id int(10) NOT NULL AUTO_INCREMENT,
  card_number bigint unique not null,
  user_id int(10),
  card_type_id int(10) not null,
  payment_system_type_id int(10) not null,
  cvv int(10) not null,
  first_name_holder varchar(64) not null,
  last_name_holder varchar(64) not null,
  code_currency int(10) not null,
  PRIMARY KEY (card_id),
  foreign key(user_id) references user(user_id)
  ON DELETE SET NULL,
  foreign key(card_type_id) references card_type(card_type_id),
  foreign key(payment_system_type_id) references payment_system_type(payment_system_type_id),
  foreign key(code_currency) references currency(code_currency)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS payment_account (
  payment_account_id int(10) NOT NULL AUTO_INCREMENT,
  payment_account_number bigint unique not null,
  card_id int(10) not null,
  balance float not null,
  status_id int(10) not null,
  PRIMARY KEY (payment_account_id),
  foreign key(status_id) references status(status_id),
  foreign key(card_id) references card(card_id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS payment_type (
  payment_type_id int(16) NOT NULL AUTO_INCREMENT,
  payment_type varchar(64) not null,
  PRIMARY KEY (payment_type_id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS payment (
  payment_id int(10) NOT NULL AUTO_INCREMENT,
  payment_account_id int(10) not null,
  date date not null,
  external_account bigint not null,
  sum float(20) not null,
  description varchar(150),
  payment_type_id int(10) not null,
  PRIMARY KEY (payment_id),
  foreign key(payment_account_id) references payment_account(payment_account_id),
  foreign key(payment_type_id) references payment_type(payment_type_id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;