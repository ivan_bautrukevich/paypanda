package by.bautruk.paybau.encryption;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author ivan.bautruvkevich
 *         01.02.2016 16:54.
 */
public class EncryptionDataTest {

    private static EncryptionData encryptionData;

    @Before
    public void init() {
        encryptionData = new EncryptionData();
    }

    @Test
    public void testGetMD5() throws Exception {
        String data = encryptionData.getMD5("hello");
        Assert.assertNotNull(data);
    }

    @After
    public void clear() {
        encryptionData = null;
    }
}