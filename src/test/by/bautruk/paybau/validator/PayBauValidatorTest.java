package by.bautruk.paybau.validator;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author ivan.bautruvkevich
 *         19.01.2016 22:22.
 */
public class PayBauValidatorTest {

    private static final String FIELD_LOGIN = "login";
    private static final String SUM = "login";

    @Test
    public void testCheckData() throws Exception {
        PayBauValidator validator = new PayBauValidator();
        assertFalse(validator.checkData(FIELD_LOGIN, "P(0."));
        assertTrue(validator.checkData(FIELD_LOGIN, "Ivan"));
        assertFalse(validator.checkData(SUM, "100.0.5"));
    }
}