package by.bautruk.paybau.pool;

import by.bautruk.paybau.exception.DAOException;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author ivan.bautruvkevich
 *         01.02.2016 16:50.
 */
public class ConnectionPoolTest {

    private static ConnectionPool connectionPool;

    @BeforeClass
    public static void initConnectionPool() {
        connectionPool = ConnectionPool.getInstance();
    }

    @Test
    public void getConnectionTest() throws DAOException {
        for (int i = 0; i < 30; i++) {
            Assert.assertNotNull(connectionPool.getConnection());
        }
    }

    @AfterClass
    public static void cleanUpConnectionPool() throws DAOException {
        connectionPool.cleanUp();
    }
}