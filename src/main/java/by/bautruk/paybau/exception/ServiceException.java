package by.bautruk.paybau.exception;

/**
 * @author ivan.bautruvkevich
 *         10.01.2016 13:38.
 */
public class ServiceException extends Exception {

    public ServiceException() {
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
