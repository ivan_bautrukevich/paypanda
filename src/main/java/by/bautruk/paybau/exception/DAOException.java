package by.bautruk.paybau.exception;

/**
 * @author ivan.bautruvkevich
 *         10.01.2016 13:38.
 */
public class DAOException extends Exception {

    public DAOException() {
    }

    public DAOException(String message) {
        super(message);
    }

    public DAOException(Throwable cause) {
        super(cause);
    }

    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }
}
