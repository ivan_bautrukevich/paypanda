package by.bautruk.paybau.exception;

/**
 * @author ivan.bautruvkevich
 *         04.02.2016 22:01.
 */
public class ApplicationException extends Exception {

    public ApplicationException() {

    }

    public ApplicationException(String message) {
        super(message);
    }

    public ApplicationException(Throwable cause) {
        super(cause);
    }

    public ApplicationException(String message, Throwable cause) {
        super(message, cause);
    }
}
