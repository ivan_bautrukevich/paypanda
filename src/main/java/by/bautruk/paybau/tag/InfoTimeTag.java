package by.bautruk.paybau.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.GregorianCalendar;

/**
 * @author ivan.bautruvkevich
 *         01.02.2016 17:18.
 */
public class InfoTimeTag extends TagSupport {

    private static final String LOCALE = "locale";

    /**
     * Defines the code which performs during the request when
     * the starting tag was found.
     * @return  an integer constant value {@code SKIP_BODY} that ignores
     *          all the information between starting and ending tag.
     * @throws  JspException if Java Server Pages' error occurs.
     */

    @Override
    public int doStartTag() throws JspException {
        String  loc = (String) pageContext.getSession().getAttribute(LOCALE);
        GregorianCalendar gc = new GregorianCalendar();
        String locale = "Locale : <b> " + loc + " </b>";
        String time = "Time : <b> " + gc.getTime() + " </b>";
        try {
            JspWriter out = pageContext.getOut();
            out.write(time + locale);
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }

    /**
     * Calls right after {@code doStartTag()} method because of integer
     * constant value {@code SKIP_BODY} that returns by {@code doStartTag()}.
     * @return  an integer constant value {@code EVAL_PAGE} that allows to
     *          carry out the following page content.
     */

    @Override
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }
}
