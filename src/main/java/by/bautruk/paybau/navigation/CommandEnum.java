package by.bautruk.paybau.navigation;

import by.bautruk.paybau.navigation.admin.*;
import by.bautruk.paybau.navigation.user.*;

/**
 * @author ivan.bautruvkevich
 *         10.01.2016 14:28.
 *         The enumeration class {@code CommandEnum} includes the command
 *         names. Each command name contains definite class, that
 *         realizes its own actions.
 */
public enum CommandEnum {

    ADD_ACCOUNT {
        /**
         * Contains {@code ActionCommand} class.
         * @return an instance of {@code AddAccountCommand} class.
         */
        @Override
        public ActionCommand getCommand() {
            return new AddAccountCommand();
        }
    },
    ADD_CARD {
        /**
         * Contains {@code ActionCommand} class.
         * @return an instance of {@code AddCardCommand} class.
         */
        @Override
        public ActionCommand getCommand() {
            return new AddCardCommand();
        }
    },
    ADD_MONEY {
        /**
         * Contains {@code ActionCommand} class.
         * @return an instance of {@code AddAccountCommand} class.
         */
        @Override
        public ActionCommand getCommand() {
            return new AddMoneyCommand();
        }
    },
    ADD_PAYMENT {
        /**
         * Contains {@code ActionCommand} class.
         * @return an instance of {@code AddPaymentCommand} class.
         */
        @Override
        public ActionCommand getCommand() {
            return new AddPaymentCommand();
        }
    },
    ASK_FORM_BLOCK {
        /**
         * Contains {@code ActionCommand} class.
         * @return an instance of {@code AddAccountCommand} class.
         */
        @Override
        public ActionCommand getCommand() {
            return new AskAccountBlockFormViewCommand();
        }
    },
    ASK_FORM_PAYMENT_FIRST {
        /**
         * Contains {@code ActionCommand} class.
         * @return an instance of {@code AddAccountCommand} class.
         */
        @Override
        public ActionCommand getCommand() {
            return new AskPaymentFirstFormViewCommand();
        }
    },
    ASK_FORM_PAYMENT_SECOND {
        /**
         * Contains {@code ActionCommand} class.
         * @return an instance of {@code AddAccountCommand} class.
         */
        @Override
        public ActionCommand getCommand() {
            return new AskPaymentSecondFormSViewCommand();
        }
    },
    ASK_FORM_CARD {
        /**
         * Contains {@code ActionCommand} class.
         * @return an instance of {@code AddAccountCommand} class.
         */
        @Override
        public ActionCommand getCommand() {
            return new AskCardFormViewCommand();
        }
    },
    ASK_FORM_ACCOUNT {
        /**
         * Contains {@code ActionCommand} class.
         * @return an instance of {@code AddAccountCommand} class.
         */
        @Override
        public ActionCommand getCommand() {
            return new AskAccountFormViewCommand();
        }
    },
    ASK_FORM_MONEY {
        /**
         * Contains {@code ActionCommand} class.
         * @return an instance of {@code AddAccountCommand} class.
         */
        @Override
        public ActionCommand getCommand() {
            return new AskMoneyFormViewCommand();
        }
    },
    ASK_FORM_UNBLOCK {
        /**
         * Contains {@code ActionCommand} class.
         * @return an instance of {@code AddAccountCommand} class.
         */
        @Override
        public ActionCommand getCommand() {
            return new AskUnblockAccountFormViewCommand();
        }
    },
    ASK_FORM_USER {
        /**
         * Contains {@code ActionCommand} class.
         * @return an instance of {@code AddAccountCommand} class.
         */
        @Override
        public ActionCommand getCommand() {
            return new AskUserFormViewCommand();
        }
    },
    BLOCK_ACCOUNT {
        /**
         * Contains {@code ActionCommand} class.
         * @return an instance of {@code BlockAccountCommand} class.
         */
        @Override
        public ActionCommand getCommand() {
            return new BlockAccountCommand();
        }
    },
    DAY_PAYMENTS {
        /**
         * Contains {@code ActionCommand} class.
         * @return an instance of {@code DayPaymentsCommand} class.
         */
        @Override
        public ActionCommand getCommand() {
            return new DayPaymentsCommand();
        }
    },
    DELETE_USERS {
        /**
         * Contains {@code ActionCommand} class.
         * @return an instance of {@code DeleteUserCommand} class.
         */
        @Override
        public ActionCommand getCommand() {
            return new DeleteUserCommand();
        }
    },
    LANGUAGE {
        /**
         * Contains {@code ActionCommand} class.
         * @return an instance of {@code LanguageCommand} class.
         */
        @Override
        public ActionCommand getCommand() {
            return new LanguageCommand();
        }
    },
    LOGIN {
        /**
         * Contains {@code ActionCommand} class.
         * @return an instance of {@code LoginCommand} class.
         */
        @Override
        public ActionCommand getCommand() {
            return new LoginCommand();
        }
    },
    LOGOUT {
        /**
         * Contains {@code ActionCommand} class.
         * @return an instance of {@code LogoutCommand} class.
         */
        @Override
        public ActionCommand getCommand() {
            return new LogoutCommand();
        }
    },
    REGISTRATION {
        /**
         * Contains {@code ActionCommand} class.
         * @return an instance of {@code RegistrationCommand} class.
         */
        @Override
        public ActionCommand getCommand() {
            return new RegistrationCommand();
        }
    },
    SIGNUP {
        /**
         * Contains {@code ActionCommand} class.
         * @return an instance of {@code SignUpCommand} class.
         */
        @Override
        public ActionCommand getCommand() {
            return new SignUpCommand();
        }
    },
    TAKE_ACCOUNTS {
        /**
         * Contains {@code ActionCommand} class.
         * @return an instance of {@code TakeAccountsCommand} class.
         */
        @Override
        public ActionCommand getCommand() {
            return new TakeAccountsCommand();
        }
    },
    TAKE_CARDS {
        /**
         * Contains {@code ActionCommand} class.
         * @return an instance of {@code TakeCardsCommand} class.
         */
        @Override
        public ActionCommand getCommand() {
            return new TakeCardsCommand();
        }
    },
    TAKE_PAYMENTS {
        /**
         * Contains {@code ActionCommand} class.
         * @return an instance of {@code TakePaymentsCommand} class.
         */
        @Override
        public ActionCommand getCommand() {
            return new TakePaymentsCommand();
        }
    },
    TAKE_PAYMENTS_DAY {
        /**
         * Contains {@code ActionCommand} class.
         * @return an instance of {@code TakePaymentsDayCommand} class.
         */
        @Override
        public ActionCommand getCommand() {
            return new TakePaymentsDayCommand();
        }
    },
    TAKE_USERS {
        /**
         * Contains {@code ActionCommand} class.
         * @return an instance of {@code TakeUsersCommand} class.
         */
        @Override
        public ActionCommand getCommand() {
            return new TakeUsersCommand();
        }
    },
    UNBLOCK_ACCOUNT {
        /**
         * Contains {@code ActionCommand} class.
         * @return an instance of {@code UnblockAccountCommand} class.
         */
        @Override
        public ActionCommand getCommand() {
            return new UnblockAccountCommand();
        }
    };

    /**
     * An abstract method that realizes inside each enumeration
     * element.
     *
     * @return an instance of definite command class.
     */
    public abstract ActionCommand getCommand();
}
