package by.bautruk.paybau.navigation;

import by.bautruk.paybau.config.ConfigManager;
import by.bautruk.paybau.exception.ApplicationException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author ivan.bautruvkevich
 *         10.01.2016 14:29.
 *         The {@code LanguageCommand} class represents an ability of manipulation
 *         with an instance of {@code HttpServletRequest}.
 */
public class LanguageCommand implements ActionCommand {

    private static final String ATTRIBUTE_LOCALE = "locale";
    private static final String PARAMETER_LANGUAGE = "lang";
    private static final String USER_ID = "userId";
    private static final String PAGE_INDEX = "page.index";
    private static final String ATTRIBUTE_PREVIOUS_COMMAND = "previousCommand";
    private static final String EN = "en";
    private static final String RU = "ru";
    private static final String ATTRIBUTE_EN = "en_US";
    private static final String ATTRIBUTE_RU = "ru_RU";
    private static final String SIGNUP = "signup";
    private static final String CONTROLLER_COMMAND = "/controller?command=";

    /**
     * Change language of page.
     *
     * @param request is an instance of {@code HttpServletRequest}.
     * @return a result page at which should be forwarded to.
     * @throws ApplicationException if there are any troubles with PaymentAccountService
     *                              if user doesn't have access for this operation.
     */

    @Override
    public String execute(HttpServletRequest request) {
        String language = request.getParameter(PARAMETER_LANGUAGE);
        HttpSession session = request.getSession();

        switch (language) {
            case EN:
                session.setAttribute(ATTRIBUTE_LOCALE, ATTRIBUTE_EN);
                break;
            case RU:
                session.setAttribute(ATTRIBUTE_LOCALE, ATTRIBUTE_RU);
                break;
        }
        Long userId = (Long) session.getAttribute(USER_ID);
        String previousCommand = (String) session.getAttribute(ATTRIBUTE_PREVIOUS_COMMAND);
        if (userId != null) {
            return CONTROLLER_COMMAND + previousCommand;
        } else if (SIGNUP.equals(previousCommand)) {
            return CONTROLLER_COMMAND + previousCommand;
        } else {
            return ConfigManager.getProperty(PAGE_INDEX);
        }
    }
}
