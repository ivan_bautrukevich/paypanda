package by.bautruk.paybau.navigation.admin;

import by.bautruk.paybau.config.ConfigManager;
import by.bautruk.paybau.config.MessageManager;
import by.bautruk.paybau.entity.Role;
import by.bautruk.paybau.entity.User;
import by.bautruk.paybau.exception.ApplicationException;
import by.bautruk.paybau.exception.ServiceException;
import by.bautruk.paybau.navigation.ActionCommand;
import by.bautruk.paybau.service.RoleService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         02.02.2016 19:29.
 *         The {@code AskUserFormViewCommand} class represents an ability of manipulation
 *         with an instance of {@code HttpServletRequest}.
 */
public class AskUserFormViewCommand implements ActionCommand {

    private static final String USER = "user";
    private static final String ROLE = "admin";
    private static final String PAGE_ADD_USER = "page.addUser";
    private static final String ATTRIBUTE_ROLES = "roles";
    private static final String ATTRIBUTE_PREVIOUS_COMMAND = "previousCommand";
    private static final String PREVIOUS_COMMAND = "ask_form_user";
    private static final String LOCALE = "locale";
    private static final String MESSAGE_ACCESS_ERROR = "message.accesserror";

    /**
     * Puts the list of roles to request's attribute. Gives a string value
     * of the page on which you can see form for creating new user.
     *
     * @param request is an instance of {@code HttpServletRequest}.
     * @return a result page at which should be forwarded to.
     * @throws ApplicationException if there are any troubles with PaymentAccountService
     *                              if user doesn't have access for this operation.
     */

    @Override
    public String execute(HttpServletRequest request) throws ApplicationException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(USER);
        if (ROLE.equals(user.getRole().getRole())) {
            try {
                RoleService roleService = new RoleService();
                List<Role> roles;
                roles = roleService.takeAll();
                request.setAttribute(ATTRIBUTE_ROLES, roles);
                session.setAttribute(ATTRIBUTE_PREVIOUS_COMMAND, PREVIOUS_COMMAND);
                return ConfigManager.getProperty(PAGE_ADD_USER);
            } catch (ServiceException e) {
                throw new ApplicationException(e);
            }
        } else {
            throw new ApplicationException(MessageManager.getProperty(MESSAGE_ACCESS_ERROR,
                    (String) session.getAttribute(LOCALE)));
        }
    }
}
