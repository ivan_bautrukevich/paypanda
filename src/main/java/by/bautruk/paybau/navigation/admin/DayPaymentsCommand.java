package by.bautruk.paybau.navigation.admin;

import by.bautruk.paybau.config.ConfigManager;
import by.bautruk.paybau.config.MessageManager;
import by.bautruk.paybau.entity.Payment;
import by.bautruk.paybau.entity.User;
import by.bautruk.paybau.exception.ApplicationException;
import by.bautruk.paybau.exception.ServiceException;
import by.bautruk.paybau.navigation.ActionCommand;
import by.bautruk.paybau.service.PaymentService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Date;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         03.02.2016 11:14.
 *         The {@code DayPaymentsCommand} class represents an ability of manipulation
 *         with an instance of {@code HttpServletRequest}.
 */
public class DayPaymentsCommand implements ActionCommand {

    private static final String USER = "user";
    private static final String ROLE = "admin";
    private static final String PARAMETER_CALENDAR = "calendar";
    private static final String ATTRIBUTE_PREVIOUS_COMMAND = "previousCommand";
    private static final String PREVIOUS_COMMAND = "day_payments";
    private static final String PAGE_PAYMENTS_DAY = "page.paymentsDay";
    private static final String ATTRIBUTE_PAYMENTS = "payments";
    private static final String LOCALE = "locale";
    private static final String MESSAGE_ACCESS_ERROR = "message.accesserror";
    private static final String ATTRIBUTE_DAY = "day";

    /**
     * Puts the list of payments by selected day  to request's attribute.
     * Gives a string value of the page on which you can this list.
     *
     * @param request is an instance of {@code HttpServletRequest}.
     * @return a result page at which should be forwarded to.
     * @throws ApplicationException if there are any troubles with PaymentAccountService
     *                              if user doesn't have access for this operation.
     */

    @Override
    public String execute(HttpServletRequest request) throws ApplicationException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(USER);
        if (ROLE.equals(user.getRole().getRole())) {
            Date date;
            try {
                date = Date.valueOf(request.getParameter(PARAMETER_CALENDAR));
            } catch (IllegalArgumentException e) {
                return ConfigManager.getProperty(PAGE_PAYMENTS_DAY);
            }
            PaymentService paymentService = new PaymentService();
            List<Payment> payments;
            try {
                payments = paymentService.takeAllByDate(date);
                request.setAttribute(ATTRIBUTE_DAY, date);
                request.setAttribute(ATTRIBUTE_PAYMENTS, payments);
                session.setAttribute(ATTRIBUTE_PREVIOUS_COMMAND, PREVIOUS_COMMAND);
                return ConfigManager.getProperty(PAGE_PAYMENTS_DAY);
            } catch (ServiceException e) {
                throw new ApplicationException(e);
            }
        } else {
            throw new ApplicationException(MessageManager.getProperty(MESSAGE_ACCESS_ERROR,
                    (String) session.getAttribute(LOCALE)));
        }
    }
}