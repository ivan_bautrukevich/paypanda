package by.bautruk.paybau.navigation.admin;

import by.bautruk.paybau.config.ConfigManager;
import by.bautruk.paybau.config.MessageManager;
import by.bautruk.paybau.entity.PaymentAccount;
import by.bautruk.paybau.entity.PaymentAccountStatus;
import by.bautruk.paybau.entity.User;
import by.bautruk.paybau.exception.ApplicationException;
import by.bautruk.paybau.exception.ServiceException;
import by.bautruk.paybau.navigation.ActionCommand;
import by.bautruk.paybau.service.PaymentAccountService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author ivan.bautruvkevich
 *         03.02.2016 15:51.
 *         The {@code UnblockAccountCommand} class represents an ability of manipulation
 *         with an instance of {@code HttpServletRequest}.
 */
public class UnblockAccountCommand implements ActionCommand {

    private static final String USER = "user";
    private static final String ROLE = "admin";
    private static final String PARAMETER_OPTION = "option";
    private static final long STATUS_UNBLOCKED_ID = 1;
    private static final String PAGE_ADMIN = "page.admin";
    private static final String ATTRIBUTE_PREVIOUS_COMMAND = "previousCommand";
    private static final String PREVIOUS_COMMAND = "login";
    private static final String ATTRIBUTE_MESSAGE = "message";
    private static final String MESSAGE_UNBLOCK = "message.unblock";
    private static final String MESSAGE_UNBLOCK_EMPTY = "message.unblockEmpty";
    private static final String LOCALE = "locale";
    private static final String MESSAGE_ACCESS_ERROR = "message.accesserror";


    /**
     * Unblocked selected payment accounts.
     *
     * @param request is an instance of {@code HttpServletRequest}.
     * @return a result page at which should be forwarded to.
     * @throws ApplicationException if there are any troubles with PaymentAccountService
     *                              if user doesn't have access for this operation.
     */

    @Override
    public String execute(HttpServletRequest request) throws ApplicationException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(USER);
        if (ROLE.equals(user.getRole().getRole())) {
            try {
                String[] options = request.getParameterValues(PARAMETER_OPTION);
                if (options != null){
                    PaymentAccountService paymentAccountService = new PaymentAccountService();
                    for (String option : options) {
                        Long paId = paymentAccountService.takeByNumber(Long.valueOf(option));
                        PaymentAccount paymentAccount = paymentAccountService.takeById(paId);
                        PaymentAccountStatus paymentAccountStatus = new PaymentAccountStatus();
                        paymentAccountStatus.setId(STATUS_UNBLOCKED_ID);
                        paymentAccount.setPaymentAccountStatus(paymentAccountStatus);
                        paymentAccountService.updateById(paId, paymentAccount);
                    }
                    request.setAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(MESSAGE_UNBLOCK,
                            (String) session.getAttribute(LOCALE)));
                }else {
                    request.setAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(MESSAGE_UNBLOCK_EMPTY,
                            (String) session.getAttribute(LOCALE)));
                }
                session.setAttribute(ATTRIBUTE_PREVIOUS_COMMAND, PREVIOUS_COMMAND);
                return ConfigManager.getProperty(PAGE_ADMIN);
            } catch (ServiceException e) {
                throw new ApplicationException(e);
            }
        } else {
            throw new ApplicationException(MessageManager.getProperty(MESSAGE_ACCESS_ERROR,
                    (String) session.getAttribute(LOCALE)));
        }
    }
}