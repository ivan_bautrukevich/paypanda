package by.bautruk.paybau.navigation.admin;

import by.bautruk.paybau.config.ConfigManager;
import by.bautruk.paybau.config.MessageManager;
import by.bautruk.paybau.entity.User;
import by.bautruk.paybau.exception.ApplicationException;
import by.bautruk.paybau.exception.ServiceException;
import by.bautruk.paybau.navigation.ActionCommand;
import by.bautruk.paybau.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author ivan.bautruvkevich
 *         17.02.2016 16:28.
 *         The {@code DeleteUserCommand} class represents an ability of manipulation
 *         with an instance of {@code HttpServletRequest}.
 */
public class DeleteUserCommand implements ActionCommand {

    private static final String USER = "user";
    private static final String ROLE = "admin";
    private static final String PARAMETER_DELETE_USERS = "deleteUsers";
    private static final String PAGE_ADMIN = "page.admin";
    private static final String ATTRIBUTE_PREVIOUS_COMMAND = "previousCommand";
    private static final String PREVIOUS_COMMAND = "login";
    private static final String ATTRIBUTE_MESSAGE = "message";
    private static final String MESSAGE_UNBLOCK = "message.unblock";
    private static final String MESSAGE_UNBLOCK_EMPTY = "message.unblockEmpty";
    private static final String LOCALE = "locale";
    private static final String MESSAGE_ACCESS_ERROR = "message.accesserror";


    /**
     * Delete selected users.
     *
     * @param request is an instance of {@code HttpServletRequest}.
     * @return a result page at which should be forwarded to.
     * @throws ApplicationException if there are any troubles with PaymentAccountService
     *                              if user doesn't have access for this operation.
     */

    @Override
    public String execute(HttpServletRequest request) throws ApplicationException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(USER);
        if (ROLE.equals(user.getRole().getRole())) {
            try {
                String[] deleteUsers = request.getParameterValues(PARAMETER_DELETE_USERS);
                if (deleteUsers != null) {
                    UserService userService = new UserService();
                    for (String deleteUser : deleteUsers) {
                        Long userId = Long.parseLong(deleteUser);
                        if (user.getId() != userId) {
                            userService.deleteById(userId);
                        }
                    }
                    request.setAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(MESSAGE_UNBLOCK,
                            (String) session.getAttribute(LOCALE)));
                } else {
                    request.setAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(MESSAGE_UNBLOCK_EMPTY,
                            (String) session.getAttribute(LOCALE)));
                }
                session.setAttribute(ATTRIBUTE_PREVIOUS_COMMAND, PREVIOUS_COMMAND);
                return ConfigManager.getProperty(PAGE_ADMIN);
            } catch (ServiceException e) {
                throw new ApplicationException(e);
            }
        } else {
            throw new ApplicationException(MessageManager.getProperty(MESSAGE_ACCESS_ERROR,
                    (String) session.getAttribute(LOCALE)));
        }
    }
}
