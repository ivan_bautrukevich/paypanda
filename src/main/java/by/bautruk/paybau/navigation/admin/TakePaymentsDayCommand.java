package by.bautruk.paybau.navigation.admin;

import by.bautruk.paybau.config.ConfigManager;
import by.bautruk.paybau.config.MessageManager;
import by.bautruk.paybau.entity.User;
import by.bautruk.paybau.exception.ApplicationException;
import by.bautruk.paybau.navigation.ActionCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author ivan.bautruvkevich
 *         03.02.2016 11:06.
 *         The {@code TakePaymentsDayCommand} class represents an ability of manipulation
 *         with an instance of {@code HttpServletRequest}.
 */
public class TakePaymentsDayCommand implements ActionCommand {

    private static final String USER = "user";
    private static final String ROLE = "admin";
    private static final String PAGE_PAYMENTS_DAY = "page.paymentsDay";
    private static final String ATTRIBUTE_PREVIOUS_COMMAND = "previousCommand";
    private static final String PREVIOUS_COMMAND = "take_payments_day";
    private static final String LOCALE = "locale";
    private static final String MESSAGE_ACCESS_ERROR = "message.accesserror";

    /**
     * Gives a string value of the page on which you can see calendar.
     * Where you can select date of report for taking report.
     *
     * @param request is an instance of {@code HttpServletRequest}.
     * @return a result page at which should be forwarded to.
     * @throws ApplicationException if there are any troubles with PaymentAccountService
     *                              if user doesn't have access for this operation.
     */
    @Override
    public String execute(HttpServletRequest request) throws ApplicationException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(USER);
        if (ROLE.equals(user.getRole().getRole())) {
            session.setAttribute(ATTRIBUTE_PREVIOUS_COMMAND, PREVIOUS_COMMAND);
            return ConfigManager.getProperty(PAGE_PAYMENTS_DAY);
        } else {
            throw new ApplicationException(MessageManager.getProperty(MESSAGE_ACCESS_ERROR,
                    (String) session.getAttribute(LOCALE)));
        }
    }
}
