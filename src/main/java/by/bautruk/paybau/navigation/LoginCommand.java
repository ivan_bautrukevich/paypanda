package by.bautruk.paybau.navigation;

import by.bautruk.paybau.config.ConfigManager;
import by.bautruk.paybau.config.MessageManager;
import by.bautruk.paybau.entity.User;
import by.bautruk.paybau.exception.ApplicationException;
import by.bautruk.paybau.exception.ServiceException;
import by.bautruk.paybau.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author ivan.bautruvkevich
 *         10.01.2016 14:29.
 *         The {@code LoginCommand} class represents an ability of manipulation
 *         with an instance of {@code HttpServletRequest}.
 */
public class LoginCommand implements ActionCommand {

    private static final String PARAM_LOGIN = "login";
    private static final String PARAM_PASSWORD = "password";
    private static final String USER = "user";
    private static final String ADMIN = "admin";
    private static final String USER_ID = "userId";
    private static final String PAGE_USER = "page.user";
    private static final String PAGE_ADMIN = "page.admin";
    private static final String PAGE_INDEX = "page.index";
    private static final String LOCALE = "locale";
    private static final String ATTRIBUTE_ERROR = "errorLoginPassMessage";
    private static final String MESSAGE_LOGIN_ERROR = "message.loginerror";
    private static final String ATTRIBUTE_PREVIOUS_COMMAND = "previousCommand";
    private static final String PREVIOUS_COMMAND = "login";

    /**
     * Check login and password. If its right compare role of user.
     * If it user forwarded to main user page, if admin forwarded to main admin page.
     *
     * @param request is an instance of {@code HttpServletRequest}.
     * @return a result page at which should be forwarded to.
     * @throws ApplicationException if there are any troubles with PaymentAccountService
     *                              if user doesn't have access for this operation.
     */

    @Override
    public String execute(HttpServletRequest request) throws ApplicationException {
        HttpSession session = request.getSession();
        String enteredLogin = request.getParameter(PARAM_LOGIN);
        String enteredPassword = request.getParameter(PARAM_PASSWORD);
        User user = (User) session.getAttribute(USER);
        session.setAttribute(ATTRIBUTE_PREVIOUS_COMMAND, PREVIOUS_COMMAND);
        if (user == null && enteredLogin != null && enteredPassword != null) {
            try {
                UserService userService = new UserService();
                User defineUser = userService.defineUser(enteredLogin, enteredPassword);
                if (defineUser.getId() != null) {
                    session.setAttribute(USER_ID, defineUser.getId());
                    session.setAttribute(USER, defineUser);
                    String role = defineUser.getRole().getRole();
                    return findPageByRole(role);
                } else {
                    request.setAttribute(ATTRIBUTE_ERROR, MessageManager.getProperty(MESSAGE_LOGIN_ERROR,
                            (String) session.getAttribute(LOCALE)));
                    return ConfigManager.getProperty(PAGE_INDEX);
                }
            } catch (ServiceException e) {
                throw new ApplicationException(e);
            }
        } else {
            String role = user.getRole().getRole();
            return findPageByRole(role);
        }
    }

    private String findPageByRole(String role) {
        switch (role) {
            case ADMIN:
                return ConfigManager.getProperty(PAGE_ADMIN);
            case USER:
                return ConfigManager.getProperty(PAGE_USER);
            default:
                return ConfigManager.getProperty(PAGE_INDEX);
        }
    }
}

