package by.bautruk.paybau.navigation;

import by.bautruk.paybau.config.ConfigManager;
import by.bautruk.paybau.config.MessageManager;
import by.bautruk.paybau.entity.Role;
import by.bautruk.paybau.entity.User;
import by.bautruk.paybau.exception.ApplicationException;
import by.bautruk.paybau.exception.ServiceException;
import by.bautruk.paybau.service.RoleService;
import by.bautruk.paybau.service.UserService;
import by.bautruk.paybau.validator.PayBauValidator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author ivan.bautruvkevich
 *         15.01.2016 22:11.
 *         The {@code LogoutCommand} class represents an ability of manipulation
 *         with an instance of {@code HttpServletRequest}.
 */
public class RegistrationCommand implements ActionCommand {

    private static final String PARAM_FIRST_NAME = "firstName";
    private static final String PARAM_LAST_NAME = "lastName";
    private static final String PARAM_EMAIL = "email";
    private static final String PARAM_LOGIN = "login";
    private static final String PARAM_PASSWORD = "password";
    private static final String PARAM_ROLE = "selectRole";
    private static final String USER = "user";
    private static final String USER_ID = "userId";
    private static final String PAGE_USER = "page.user";
    private static final String PAGE_ADMIN = "page.admin";
    private static final String PAGE_SIGN_UP = "page.signup";
    private static final String PAGE_ADD_USER = "page.addUser";
    private static final String LOCALE = "locale";
    private static final String ATTRIBUTE_MESSAGE = "message";
    private static final String MESSAGE_VALID = "message.valid";
    private static final String MESSAGE_OCCUPIED_LOGIN = "message.occupiedlogin";
    private static final String ATTRIBUTE_PREVIOUS_COMMAND = "previousCommand";
    private static final String PREVIOUS_COMMAND_USER = "login";
    private static final String PREVIOUS_COMMAND_ADMIN = "login";

    /**
     * Add new user. Validate attributes from request. This method use admin and user.
     * If admin add new user forwarded to main admin page, if user forwarded to
     * main user page.
     *
     * @param request is an instance of {@code HttpServletRequest}.
     * @return a result page at which should be forwarded to.
     * @throws ApplicationException if there are any troubles with PaymentAccountService
     *                              if user doesn't have access for this operation.
     */

    @Override
    public String execute(HttpServletRequest request) throws ApplicationException {
        UserService userService = new UserService();
        HttpSession session = request.getSession();
        User user = new User();
        String firstName = request.getParameter(PARAM_FIRST_NAME);
        String lastName = request.getParameter(PARAM_LAST_NAME);
        String email = request.getParameter(PARAM_EMAIL);
        String login = request.getParameter(PARAM_LOGIN);
        String password = request.getParameter(PARAM_PASSWORD);
        String role = request.getParameter(PARAM_ROLE);
        String previousCommand;
        String page;
        String errorPage;
        String setRole;
        if (role == null) {
            setRole = USER;
            page = PAGE_USER;
            errorPage = PAGE_SIGN_UP;
            previousCommand = PREVIOUS_COMMAND_USER;
        } else {
            setRole = role;
            page = PAGE_ADMIN;
            errorPage = PAGE_ADD_USER;
            previousCommand = PREVIOUS_COMMAND_ADMIN;
        }
        session.setAttribute(ATTRIBUTE_PREVIOUS_COMMAND, previousCommand);
        PayBauValidator valid = new PayBauValidator();
        if (valid.checkData(PARAM_FIRST_NAME, firstName) && valid.checkData(PARAM_LAST_NAME, lastName) &&
                valid.checkData(PARAM_EMAIL, email) && valid.checkData(PARAM_LOGIN, login) &&
                valid.checkData(PARAM_PASSWORD, password)) {
            try {
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setEmail(email);
                user.setLogin(login);
                Long checkLogin = userService.defineByLogin(login);
                if (checkLogin == null) {
                    user.setPassword(password);
                    RoleService roleService = new RoleService();
                    Role userRole = roleService.defineByName(setRole);
                    user.setRole(userRole);
                    userService.add(user);
                    user = userService.defineUser(login, password);
                    if (role == null) {
                        Long userId = user.getId();
                        session.setAttribute(USER_ID, userId);
                        user.setLogin(login);
                        user.setLogin(password);
                        request.setAttribute(USER, user);
                        session.setAttribute(USER, user);
                    }
                    return ConfigManager.getProperty(page);
                } else {
                    request.setAttribute(ATTRIBUTE_MESSAGE, login + MessageManager.getProperty(MESSAGE_OCCUPIED_LOGIN,
                            (String) session.getAttribute(LOCALE)));
                    return ConfigManager.getProperty(errorPage);
                }
            } catch (ServiceException e) {
                throw new ApplicationException(e);
            }
        } else {
            request.setAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(MESSAGE_VALID,
                    (String) session.getAttribute(LOCALE)));
            return ConfigManager.getProperty(errorPage);
        }
    }
}
