package by.bautruk.paybau.navigation.user;

import by.bautruk.paybau.config.ConfigManager;
import by.bautruk.paybau.config.MessageManager;
import by.bautruk.paybau.entity.PaymentAccount;
import by.bautruk.paybau.entity.User;
import by.bautruk.paybau.exception.ApplicationException;
import by.bautruk.paybau.exception.ServiceException;
import by.bautruk.paybau.navigation.ActionCommand;
import by.bautruk.paybau.service.PaymentAccountService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         22.01.2016 14:23.
 *         The {@code TakeAccountsCommand} class represents an ability of manipulation
 *         with an instance of {@code HttpServletRequest}.
 */
public class TakeAccountsCommand implements ActionCommand {

    private static final String ATTRIBUTE_USER = "user";
    private static final String ROLE = "user";
    private static final String PAGE_ACCOUNTS = "page.accounts";
    private static final String ATTRIBUTE_PREVIOUS_COMMAND = "previousCommand";
    private static final String PREVIOUS_COMMAND = "take_accounts";
    private static final String USER_ID = "userId";
    private static final String ATTRIBUTE_ACCOUNTS = "accounts";
    private static final String LOCALE = "locale";
    private static final String MESSAGE_ACCESS_ERROR = "message.accesserror";

    /**
     * Puts the list of payment accounts to request's attribute. Gives a string value
     * of the page on which you can see this list.
     *
     * @param request is an instance of {@code HttpServletRequest}.
     * @return a result page at which should be forwarded to.
     * @throws ApplicationException if there are any troubles with PaymentAccountService
     *                              if user doesn't have access for this operation.
     */

    @Override
    public String execute(HttpServletRequest request) throws ApplicationException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(ATTRIBUTE_USER);
        session.setAttribute(ATTRIBUTE_PREVIOUS_COMMAND, PREVIOUS_COMMAND);
        if (ROLE.equals(user.getRole().getRole())) {
            try {
                String userIdString = String.valueOf(session.getAttribute(USER_ID));
                Long userId = Long.parseLong(userIdString);
                PaymentAccountService paymentAccountService = new PaymentAccountService();
                List<PaymentAccount> accounts;
                accounts = paymentAccountService.takeAllByUserId(userId);
                request.setAttribute(ATTRIBUTE_ACCOUNTS, accounts);
                return ConfigManager.getProperty(PAGE_ACCOUNTS);
            } catch (ServiceException e) {
                throw new ApplicationException(e);
            }
        } else {
            throw new ApplicationException(MessageManager.getProperty(MESSAGE_ACCESS_ERROR,
                    (String) session.getAttribute(LOCALE)));
        }
    }
}
