package by.bautruk.paybau.navigation.user;

import by.bautruk.paybau.config.ConfigManager;
import by.bautruk.paybau.config.MessageManager;
import by.bautruk.paybau.entity.PaymentAccount;
import by.bautruk.paybau.entity.PaymentAccountStatus;
import by.bautruk.paybau.entity.User;
import by.bautruk.paybau.exception.ApplicationException;
import by.bautruk.paybau.exception.ServiceException;
import by.bautruk.paybau.navigation.ActionCommand;
import by.bautruk.paybau.service.PaymentAccountService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author ivan.bautruvkevich
 *         31.01.2016 13:49.
 *         The {@code BlockAccountCommand} class represents an ability of manipulation
 *         with an instance of {@code HttpServletRequest}.
 */
public class BlockAccountCommand implements ActionCommand {

    private static final String ATTRIBUTE_USER = "user";
    private static final String ROLE = "user";
    private static final String PAGE_USER = "page.user";
    private static final String ATTRIBUTE_PREVIOUS_COMMAND = "previousCommand";
    private static final String PREVIOUS_COMMAND = "block_account";
    private static final String SELECT_ACCOUNT = "selectAccount";
    private static final long STATUS_BLOCK_ID = 2;
    private static final String ATTRIBUTE_MESSAGE = "message";
    private static final String LOCALE = "locale";
    private static final String MESSAGE_BLOCK_ACCOUNT = "message.blockedAccount";
    private static final String MESSAGE_ACCESS_ERROR = "message.accesserror";

    /**
     * Blocked payment account.
     *
     * @param request is an instance of {@code HttpServletRequest}.
     * @return a result page at which should be forwarded to.
     * @throws ApplicationException if there are any troubles with PaymentAccountService
     *                              if user doesn't have access for this operation.
     */

    @Override
    public String execute(HttpServletRequest request) throws ApplicationException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(ATTRIBUTE_USER);
        session.setAttribute(ATTRIBUTE_PREVIOUS_COMMAND, PREVIOUS_COMMAND);
        if (ROLE.equals(user.getRole().getRole())) {
            try {
                String number = request.getParameter(SELECT_ACCOUNT);
                PaymentAccountService paymentAccountService = new PaymentAccountService();
                PaymentAccount paymentAccount;
                Long accountId;
                accountId = paymentAccountService.takeByNumber(Long.parseLong(number));
                paymentAccount = paymentAccountService.takeById(accountId);
                PaymentAccountStatus paymentAccountStatus = new PaymentAccountStatus();
                paymentAccountStatus.setId(STATUS_BLOCK_ID);
                paymentAccount.setPaymentAccountStatus(paymentAccountStatus);
                paymentAccountService.updateById(accountId, paymentAccount);
                request.setAttribute(ATTRIBUTE_MESSAGE, number + MessageManager.getProperty(MESSAGE_BLOCK_ACCOUNT,
                        (String) session.getAttribute(LOCALE)));
                return ConfigManager.getProperty(PAGE_USER);
            } catch (ServiceException e) {
                throw new ApplicationException(e);
            }
        } else {
            throw new ApplicationException(MessageManager.getProperty(MESSAGE_ACCESS_ERROR,
                    (String) session.getAttribute(LOCALE)));
        }
    }
}