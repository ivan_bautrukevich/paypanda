package by.bautruk.paybau.navigation.user;

import by.bautruk.paybau.config.ConfigManager;
import by.bautruk.paybau.config.MessageManager;
import by.bautruk.paybau.entity.*;
import by.bautruk.paybau.exception.ApplicationException;
import by.bautruk.paybau.exception.ServiceException;
import by.bautruk.paybau.navigation.ActionCommand;
import by.bautruk.paybau.service.CardService;
import by.bautruk.paybau.service.PaymentAccountService;
import by.bautruk.paybau.service.PaymentService;
import by.bautruk.paybau.service.PaymentTypeService;
import by.bautruk.paybau.validator.PayBauValidator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Date;

/**
 * @author ivan.bautruvkevich
 *         18.01.2016 13:13.
 *         The {@code AddPaymentCommand} class represents an ability of manipulation
 *         with an instance of {@code HttpServletRequest}.
 */
public class AddPaymentCommand implements ActionCommand {

    private static final String ATTRIBUTE_USER = "user";
    private static final String ROLE = "user";
    private static final String PAGE_USER = "page.user";
    private static final String ATTRIBUTE_PREVIOUS_COMMAND = "previousCommand";
    private static final String PREVIOUS_COMMAND = "login";
    private static final String MESSAGE_ENOUGH_MONEY = "message.enoughMoney";
    private static final String MESSAGE_WRONG_CVV = "message.wrongCvv";
    private static final String MESSAGE_VALID = "message.valid";
    private static final String MESSAGE_ADD_PAYMENT = "message.addPayment";
    private static final String ATTRIBUTE_MESSAGE = "message";
    private static final String LOCALE = "locale";
    private static final String PARAM_SELECT_CARD = "selectCard";
    private static final String PARAM_EXTERNAL_ACCOUNT = "externalAccount";
    private static final String PARAM_SUM = "sum";
    private static final String PARAM_DESCRIPTION = "description";
    private static final String PARAM_SELECT_TYPE = "selectType";
    private static final String PARAM_CVV = "cvv";
    private static final String ACCOUNT = "account";
    private static final String MESSAGE_ACCESS_ERROR = "message.accesserror";

    /**
     * Create a new payment account in payment system.
     *
     * @param request is an instance of {@code HttpServletRequest}.
     * @return a result page at which should be forwarded to.
     * @throws ApplicationException if there are any troubles with PaymentAccountService
     *                              if user doesn't have access for this operation.
     */

    @Override
    public String execute(HttpServletRequest request) throws ApplicationException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(ATTRIBUTE_USER);
        session.setAttribute(ATTRIBUTE_PREVIOUS_COMMAND, PREVIOUS_COMMAND);
        if (ROLE.equals(user.getRole().getRole())) {
            PaymentService paymentService = new PaymentService();
            Payment payment = new Payment();
            String selectCard = request.getParameter(PARAM_SELECT_CARD);
            String externalAccount = request.getParameter(PARAM_EXTERNAL_ACCOUNT);
            String sum = request.getParameter(PARAM_SUM);
            String description = request.getParameter(PARAM_DESCRIPTION);
            String selectType = request.getParameter(PARAM_SELECT_TYPE);
            String cvv = request.getParameter(PARAM_CVV);

            PayBauValidator valid = new PayBauValidator();
            if (valid.checkData(ACCOUNT, externalAccount) && valid.checkData(PARAM_CVV, cvv) && valid.checkData(PARAM_SUM, sum)
                    && valid.checkData(PARAM_DESCRIPTION, description)) {
                try {
                    Card card;
                    CardService cardService = new CardService();
                    card = cardService.takeByNumber(Long.parseLong(selectCard));
                    PaymentType paymentType;
                    PaymentTypeService paymentTypeService = new PaymentTypeService();
                    paymentType = paymentTypeService.takeByName(selectType);
                    PaymentAccount paymentAccount;
                    PaymentAccountService paymentAccountService = new PaymentAccountService();
                    paymentAccount = paymentAccountService.takeByCardId(card.getId());
                    Float result = paymentAccount.getBalance() - Float.parseFloat(sum);
                    if (card.getCvv() == Integer.parseInt(cvv)) {
                        if (result >= 0) {
                            payment.setPaymentAccount(paymentAccount);
                            payment.setDate(new Date(new java.util.Date().getTime()));
                            payment.setExternalPaymentAccount(Long.parseLong(externalAccount));
                            payment.setSum(Float.parseFloat(sum));
                            payment.setPaymentType(paymentType);
                            payment.setDescription(description);
                            paymentAccount.setBalance(result);
                            paymentService.add(payment);
                            Long paymentAccountId = paymentAccount.getId();
                            paymentAccountService.updateById(paymentAccountId, paymentAccount);

                            request.setAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(MESSAGE_ADD_PAYMENT,
                                    (String) session.getAttribute(LOCALE)));
                            return ConfigManager.getProperty(PAGE_USER);
                        } else {
                            request.setAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(MESSAGE_ENOUGH_MONEY,
                                    (String) session.getAttribute(LOCALE)));
                            return ConfigManager.getProperty(PAGE_USER);
                        }

                    } else {
                        request.setAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(MESSAGE_WRONG_CVV,
                                (String) session.getAttribute(LOCALE)));
                        return ConfigManager.getProperty(PAGE_USER);
                    }
                } catch (ServiceException e) {
                    throw new ApplicationException(e);
                }
            } else {
                request.setAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(MESSAGE_VALID,
                        (String) session.getAttribute(LOCALE)));
                return ConfigManager.getProperty(PAGE_USER);
            }
        } else {
            throw new ApplicationException(MessageManager.getProperty(MESSAGE_ACCESS_ERROR,
                    (String) session.getAttribute(LOCALE)));
        }
    }
}
