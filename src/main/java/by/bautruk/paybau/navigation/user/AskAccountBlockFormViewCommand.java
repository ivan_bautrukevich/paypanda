package by.bautruk.paybau.navigation.user;

import by.bautruk.paybau.config.ConfigManager;
import by.bautruk.paybau.config.MessageManager;
import by.bautruk.paybau.entity.PaymentAccount;
import by.bautruk.paybau.entity.User;
import by.bautruk.paybau.exception.ApplicationException;
import by.bautruk.paybau.exception.ServiceException;
import by.bautruk.paybau.navigation.ActionCommand;
import by.bautruk.paybau.service.PaymentAccountService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         31.01.2016 13:45.
 *         The {@code AddPaymentCommand} class represents an ability of manipulation
 *         with an instance of {@code HttpServletRequest}.
 */
public class AskAccountBlockFormViewCommand implements ActionCommand {

    private static final String ATTRIBUTE_USER = "user";
    private static final String ROLE = "user";
    private static final String PAGE_BLOCK = "page.block";
    private static final String ATTRIBUTE_PREVIOUS_COMMAND = "previousCommand";
    private static final String PREVIOUS_COMMAND = "ask_form_block";
    private static final String USER_ID = "userId";
    private static final String ATTRIBUTE_ACCOUNTS = "accounts";
    private static final String LOCALE = "locale";
    private static final long DEFAULT_UNBLOCKED_STATUS_ID = 1;
    private static final String MESSAGE_ACCESS_ERROR = "message.accesserror";

    /**
     * Forwarded page with with form for blocked payment account.
     *
     * @param request is an instance of {@code HttpServletRequest}.
     * @return a result page at which should be forwarded to.
     * @throws ApplicationException if there are any troubles with PaymentAccountService
     *                              if user doesn't have access for this operation.
     */

    @Override
    public String execute(HttpServletRequest request) throws ApplicationException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(ATTRIBUTE_USER);
        session.setAttribute(ATTRIBUTE_PREVIOUS_COMMAND, PREVIOUS_COMMAND);
        if (ROLE.equals(user.getRole().getRole())) {
            try {
                PaymentAccountService paymentAccountService = new PaymentAccountService();
                String userIdString = String.valueOf(session.getAttribute(USER_ID));
                Long userId = Long.parseLong(userIdString);
                List<PaymentAccount> accounts = paymentAccountService.takeByUserIdAndStatusId(userId,
                        DEFAULT_UNBLOCKED_STATUS_ID);
                request.setAttribute(ATTRIBUTE_ACCOUNTS, accounts);
                return ConfigManager.getProperty(PAGE_BLOCK);
            } catch (ServiceException e) {
                throw new ApplicationException(e);
            }
        } else {
            throw new ApplicationException(MessageManager.getProperty(MESSAGE_ACCESS_ERROR,
                    (String) session.getAttribute(LOCALE)));
        }
    }
}
