package by.bautruk.paybau.navigation.user;

import by.bautruk.paybau.config.ConfigManager;
import by.bautruk.paybau.config.MessageManager;
import by.bautruk.paybau.entity.PaymentAccount;
import by.bautruk.paybau.entity.User;
import by.bautruk.paybau.exception.ApplicationException;
import by.bautruk.paybau.exception.ServiceException;
import by.bautruk.paybau.navigation.ActionCommand;
import by.bautruk.paybau.service.PaymentAccountService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         22.01.2016 16:29.
 *         The {@code AskCardFormViewCommand} class represents an ability of manipulation
 *         with an instance of {@code HttpServletRequest}.
 */
public class AskMoneyFormViewCommand implements ActionCommand {

    private static final String ATTRIBUTE_USER = "user";
    private static final String ROLE = "user";
    private static final String PAGE_MONEY = "page.money";
    private static final String ATTRIBUTE_PREVIOUS_COMMAND = "previousCommand";
    private static final String PREVIOUS_COMMAND = "ask_form_money";
    private static final String USER_ID = "userId";
    private static final String ATTRIBUTE_ACCOUNTS = "accounts";
    private static final String LOCALE = "locale";
    private static final String MESSAGE_ACCESS_ERROR = "message.accesserror";

    /**
     * Forwarded page with with form for refilling money to payment account.
     *
     * @param request is an instance of {@code HttpServletRequest}.
     * @return a result page at which should be forwarded to.
     * @throws ApplicationException if there are any troubles with PaymentAccountService
     *                              if user doesn't have access for this operation.
     */

    @Override
    public String execute(HttpServletRequest request) throws ApplicationException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(ATTRIBUTE_USER);
        session.setAttribute(ATTRIBUTE_PREVIOUS_COMMAND, PREVIOUS_COMMAND);
        if (ROLE.equals(user.getRole().getRole())) {
            try {
                PaymentAccountService paymentAccountService = new PaymentAccountService();
                String userIdString = String.valueOf(session.getAttribute(USER_ID));
                Long userId = Long.valueOf(userIdString);
                List<PaymentAccount> accounts = paymentAccountService.takeAllByUserId(userId);
                request.setAttribute(ATTRIBUTE_ACCOUNTS, accounts);
                return ConfigManager.getProperty(PAGE_MONEY);
            } catch (ServiceException e) {
                throw new ApplicationException(e);
            }
        } else {
            throw new ApplicationException(MessageManager.getProperty(MESSAGE_ACCESS_ERROR,
                    (String) session.getAttribute(LOCALE)));
        }
    }
}

