package by.bautruk.paybau.navigation.user;

import by.bautruk.paybau.config.ConfigManager;
import by.bautruk.paybau.config.MessageManager;
import by.bautruk.paybau.entity.Card;
import by.bautruk.paybau.entity.PaymentAccount;
import by.bautruk.paybau.entity.User;
import by.bautruk.paybau.exception.ApplicationException;
import by.bautruk.paybau.exception.ServiceException;
import by.bautruk.paybau.navigation.ActionCommand;
import by.bautruk.paybau.service.CardService;
import by.bautruk.paybau.service.PaymentAccountService;
import by.bautruk.paybau.validator.PayBauValidator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author ivan.bautruvkevich
 *         26.01.2016 20:36.
 *         The {@code AddAccountCommand} class represents an ability of manipulation
 *         with an instance of {@code HttpServletRequest}.
 */
public class AddAccountCommand implements ActionCommand {

    private static final String ATTRIBUTE_USER = "user";
    private static final String ROLE = "user";
    private static final String ATTRIBUTE_NEW_ACCOUNT = "newAccount";
    private static final String ATTRIBUTE_SELECT_CARD = "selectCard";
    private static final String ATTRIBUTE_ACCOUNT = "account";
    private static final String ATTRIBUTE_MESSAGE = "message";
    private static final String MESSAGE_ADD_ACCOUNT = "message.addAccount";
    private static final String MESSAGE_VALID = "message.valid";
    private static final String MESSAGE_OCCUPIED_ACCOUNT = "message.occupiedAccount";
    private static final String LOCALE = "locale";
    private static final String PAGE_USER = "page.user";
    private static final String ATTRIBUTE_PREVIOUS_COMMAND = "previousCommand";
    private static final String PREVIOUS_COMMAND = "login";
    private static final String MESSAGE_ACCESS_ERROR = "message.accesserror";

    /**
     * Create a new payment account in payment system.
     *
     * @param request is an instance of {@code HttpServletRequest}.
     * @return a result page at which should be forwarded to.
     * @throws ApplicationException if there are any troubles with PaymentAccountService
     *                              if user doesn't have access for this operation.
     */

    @Override
    public String execute(HttpServletRequest request) throws ApplicationException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(ATTRIBUTE_USER);
        if (ROLE.equals(user.getRole().getRole())) {
            session.setAttribute(ATTRIBUTE_PREVIOUS_COMMAND, PREVIOUS_COMMAND );
            String accountNumber = request.getParameter(ATTRIBUTE_NEW_ACCOUNT);
            String selectCard = request.getParameter(ATTRIBUTE_SELECT_CARD);
            PayBauValidator valid = new PayBauValidator();
            if (valid.checkData(ATTRIBUTE_ACCOUNT, accountNumber)) {
                try {
                    CardService cardService = new CardService();
                    Card card = cardService.takeByNumber(Long.parseLong(selectCard));
                    PaymentAccount paymentAccount = new PaymentAccount();
                    paymentAccount.setCard(card);
                    paymentAccount.setNumber(Long.parseLong(accountNumber));
                    PaymentAccountService paymentAccountService = new PaymentAccountService();
                    Long paymentAccountCheck = paymentAccountService.takeByNumber(Long.parseLong(accountNumber));
                    if (paymentAccountCheck == null) {
                        paymentAccountService.add(paymentAccount);
                        request.setAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(MESSAGE_ADD_ACCOUNT,
                                (String) session.getAttribute(LOCALE)));
                        return ConfigManager.getProperty(PAGE_USER);
                    } else {
                        request.setAttribute(ATTRIBUTE_MESSAGE, accountNumber + " " + MessageManager.getProperty(MESSAGE_OCCUPIED_ACCOUNT,
                                (String) session.getAttribute(LOCALE)));
                        return ConfigManager.getProperty(PAGE_USER);
                    }
                } catch (ServiceException e) {
                    throw new ApplicationException(e);
                }
            } else {
                request.setAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(MESSAGE_VALID,
                        (String) session.getAttribute(LOCALE)));
                return ConfigManager.getProperty(PAGE_USER);
            }
        } else {
            throw new ApplicationException(MessageManager.getProperty(MESSAGE_ACCESS_ERROR,
                    (String) session.getAttribute(LOCALE)));
        }
    }
}
