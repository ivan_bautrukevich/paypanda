package by.bautruk.paybau.navigation.user;

import by.bautruk.paybau.config.ConfigManager;
import by.bautruk.paybau.config.MessageManager;
import by.bautruk.paybau.entity.PaymentAccount;
import by.bautruk.paybau.entity.User;
import by.bautruk.paybau.exception.ApplicationException;
import by.bautruk.paybau.exception.ServiceException;
import by.bautruk.paybau.navigation.ActionCommand;
import by.bautruk.paybau.service.PaymentAccountService;
import by.bautruk.paybau.validator.PayBauValidator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author ivan.bautruvkevich
 *         22.01.2016 16:06.
 *         The {@code AddMoneyCommand} class represents an ability of manipulation
 *         with an instance of {@code HttpServletRequest}.
 */
public class AddMoneyCommand implements ActionCommand {

    private static final String ATTRIBUTE_USER = "user";
    private static final String ROLE = "user";
    private static final String LOCALE = "locale";
    private static final String PAGE_USER = "page.user";
    private static final String ATTRIBUTE_MESSAGE = "message";
    private static final String MESSAGE_ADD_MONEY = "message.addMoney";
    private static final String MESSAGE_VALID = "message.valid";
    private static final String ATTRIBUTE_PREVIOUS_COMMAND = "previousCommand";
    private static final String PREVIOUS_COMMAND = "login";
    private static final String SUM = "sum";
    private static final String SELECT_ACCOUNT = "selectAccount";
    private static final String MESSAGE_ACCESS_ERROR = "message.accesserror";

    /**
     * Refill money to payment account.
     *
     * @param request is an instance of {@code HttpServletRequest}.
     * @return a result page at which should be forwarded to.
     * @throws ApplicationException if there are any troubles with PaymentAccountService
     *                              if user doesn't have access for this operation.
     */

    @Override
    public String execute(HttpServletRequest request) throws ApplicationException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(ATTRIBUTE_USER);
        session.setAttribute(ATTRIBUTE_PREVIOUS_COMMAND, PREVIOUS_COMMAND);
        if (ROLE.equals(user.getRole().getRole())) {
            PaymentAccount paymentAccount;
            Long number = Long.valueOf(request.getParameter(SELECT_ACCOUNT));
            String addToBalanceSum = String.valueOf(request.getParameter(SUM));
            PayBauValidator valid = new PayBauValidator();
            if (valid.checkData(SUM, addToBalanceSum)) {
                try {
                    float addToBalance = Float.parseFloat(addToBalanceSum);
                    PaymentAccountService paymentAccountService = new PaymentAccountService();
                    Long accountId = paymentAccountService.takeByNumber(number);
                    paymentAccount = paymentAccountService.takeById(accountId);
                    float oldBalance = paymentAccount.getBalance();
                    float updateBalance = oldBalance + addToBalance;
                    paymentAccount.setBalance(updateBalance);
                    paymentAccountService.updateById(accountId, paymentAccount);
                    request.setAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(MESSAGE_ADD_MONEY,
                            (String) session.getAttribute(LOCALE)));
                    return ConfigManager.getProperty(PAGE_USER);
                } catch (ServiceException e) {
                    throw new ApplicationException(e);
                }
            } else {
                request.setAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(MESSAGE_VALID,
                        (String) session.getAttribute(LOCALE)));
                return ConfigManager.getProperty(PAGE_USER);
            }
        } else {
            throw new ApplicationException(MessageManager.getProperty(MESSAGE_ACCESS_ERROR,
                    (String) session.getAttribute(LOCALE)));
        }
    }
}