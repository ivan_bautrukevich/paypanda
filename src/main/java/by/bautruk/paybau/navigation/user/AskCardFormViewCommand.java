package by.bautruk.paybau.navigation.user;

import by.bautruk.paybau.config.ConfigManager;
import by.bautruk.paybau.config.MessageManager;
import by.bautruk.paybau.entity.CardType;
import by.bautruk.paybau.entity.CurrencyType;
import by.bautruk.paybau.entity.PaymentSystemType;
import by.bautruk.paybau.entity.User;
import by.bautruk.paybau.exception.ApplicationException;
import by.bautruk.paybau.exception.ServiceException;
import by.bautruk.paybau.navigation.ActionCommand;
import by.bautruk.paybau.service.CardTypeService;
import by.bautruk.paybau.service.CurrencyTypeService;
import by.bautruk.paybau.service.PaymentSystemTypeService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         22.01.2016 21:29.
 *         The {@code AskCardFormViewCommand} class represents an ability of manipulation
 *         with an instance of {@code HttpServletRequest}.
 */

public class AskCardFormViewCommand implements ActionCommand {

    private static final String ATTRIBUTE_USER = "user";
    private static final String ROLE = "user";
    private static final String PAGE_CARD = "page.card";
    private static final String ATTRIBUTE_PREVIOUS_COMMAND = "previousCommand";
    private static final String PREVIOUS_COMMAND = "ask_form_card";
    private static final String ATTRIBUTE_PAY_SYST_TYPES = "paymentSystemTypes";
    private static final String ATTRIBUTE_CARD_TYPES = "cardTypes";
    private static final String ATTRIBUTE_CURRENCIES = "currencies";
    private static final String LOCALE = "locale";
    private static final String MESSAGE_ACCESS_ERROR = "message.accesserror";

    /**
     * Forwarded page with with form for adding new card.
     *
     * @param request is an instance of {@code HttpServletRequest}.
     * @return a result page at which should be forwarded to.
     * @throws ApplicationException if there are any troubles with PaymentAccountService
     *                              if user doesn't have access for this operation.
     */

    @Override
    public String execute(HttpServletRequest request) throws ApplicationException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(ATTRIBUTE_USER);
        session.setAttribute(ATTRIBUTE_PREVIOUS_COMMAND, PREVIOUS_COMMAND);
        if (ROLE.equals(user.getRole().getRole())) {
            try {
                PaymentSystemTypeService paymentSystemTypeService = new PaymentSystemTypeService();
                List<PaymentSystemType> paymentSystemTypes;
                paymentSystemTypes = paymentSystemTypeService.takeAll();
                request.setAttribute(ATTRIBUTE_PAY_SYST_TYPES, paymentSystemTypes);

                CardTypeService cardTypeService = new CardTypeService();
                List<CardType> cardTypes;
                cardTypes = cardTypeService.takeAll();
                request.setAttribute(ATTRIBUTE_CARD_TYPES, cardTypes);

                CurrencyTypeService currencyType = new CurrencyTypeService();
                List<CurrencyType> currencyTypes;
                currencyTypes = currencyType.takeAll();
                request.setAttribute(ATTRIBUTE_CURRENCIES, currencyTypes);
                return ConfigManager.getProperty(PAGE_CARD);
            } catch (ServiceException e) {
                throw new ApplicationException(e);
            }
        } else {
            throw new ApplicationException(MessageManager.getProperty(MESSAGE_ACCESS_ERROR,
                    (String) session.getAttribute(LOCALE)));
        }
    }
}

