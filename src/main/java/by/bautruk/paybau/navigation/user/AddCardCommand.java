package by.bautruk.paybau.navigation.user;

import by.bautruk.paybau.config.ConfigManager;
import by.bautruk.paybau.config.MessageManager;
import by.bautruk.paybau.entity.*;
import by.bautruk.paybau.exception.ApplicationException;
import by.bautruk.paybau.exception.ServiceException;
import by.bautruk.paybau.navigation.ActionCommand;
import by.bautruk.paybau.service.CardService;
import by.bautruk.paybau.service.CardTypeService;
import by.bautruk.paybau.service.CurrencyTypeService;
import by.bautruk.paybau.service.PaymentSystemTypeService;
import by.bautruk.paybau.validator.PayBauValidator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author ivan.bautruvkevich
 *         23.01.2016 11:29.
 *         The {@code AddCardCommand} class represents an ability of manipulation
 *         with an instance of {@code HttpServletRequest}.
 */
public class AddCardCommand implements ActionCommand {

    private static final String ATTRIBUTE_USER = "user";
    private static final String ROLE = "user";
    private static final String ATTRIBUTE_MESSAGE = "message";
    private static final String LOCALE = "locale";
    private static final String PAGE_USER = "page.user";
    private static final String ATTRIBUTE_PREVIOUS_COMMAND = "previousCommand";
    private static final String PREVIOUS_COMMAND = "login";
    private static final String CVV = "cvv";
    private static final String CARD_NUMBER = "cardNumber";
    private static final String SELECT_CARD_TYPE = "selectCardType";
    private static final String SELECT_PAYMENT_SYSTEM_TYPE = "selectPaymentSystemType";
    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";
    private static final String SELECT_CURRENCY = "selectCurrency";
    private static final String MESSAGE_ADD_CARD = "message.addCard";
    private static final String MESSAGE_VALID = "message.valid";
    private static final String MESSAGE_OCCUPIED_CARD = "message.occupiedCard";
    private static final String MESSAGE_ACCESS_ERROR = "message.accesserror";

    /**
     * Registers a new payment card in payment system.
     *
     * @param request is an instance of {@code HttpServletRequest}.
     * @return a result page at which should be forwarded to.
     * @throws ApplicationException if there are any troubles with PaymentAccountService
     *                              if user doesn't have access for this operation.
     */

    @Override
    public String execute(HttpServletRequest request) throws ApplicationException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(ATTRIBUTE_USER);
        session.setAttribute(ATTRIBUTE_PREVIOUS_COMMAND, PREVIOUS_COMMAND);
        if (ROLE.equals(user.getRole().getRole())) {
            String cardNumber = request.getParameter(CARD_NUMBER);
            String selectCardType = request.getParameter(SELECT_CARD_TYPE);
            String selectPaymentSystemType = request.getParameter(SELECT_PAYMENT_SYSTEM_TYPE);
            String cvv = request.getParameter(CVV);
            String firstName = request.getParameter(FIRST_NAME);
            String lastName = request.getParameter(LAST_NAME);
            String selectCurrency = request.getParameter(SELECT_CURRENCY);
            PayBauValidator valid = new PayBauValidator();

            if (valid.checkData(CARD_NUMBER, cardNumber) && valid.checkData(CVV, cvv) && valid.checkData(FIRST_NAME, firstName)
                    && valid.checkData(LAST_NAME, lastName)) {
                try {
                    CardType cardType;
                    CardTypeService cardTypeService = new CardTypeService();
                    cardType = cardTypeService.defineByType(selectCardType);

                    PaymentSystemTypeService paymentSystemTypeService = new PaymentSystemTypeService();
                    PaymentSystemType paymentSystemType;
                    paymentSystemType = paymentSystemTypeService.defineByName(selectPaymentSystemType);

                    CurrencyTypeService currencyTypeService = new CurrencyTypeService();
                    CurrencyType currencyType;
                    currencyType = currencyTypeService.defineByName(selectCurrency);

                    CardService cardService = new CardService();
                    Card card = new Card();
                    Card cardCheck = cardService.takeByNumber(Long.parseLong(cardNumber));
                    if (cardCheck.getNumber() == 0) {
                        card.setNumber(Long.parseLong(cardNumber));
                        card.setPaymentSystemType(paymentSystemType);
                        card.setCardType(cardType);
                        card.setCvv(Integer.parseInt(cvv));
                        card.setUser(user);
                        card.setFirstNameHolder(firstName);
                        card.setLastNameHolder(lastName);
                        card.setCurrencyType(currencyType);
                        cardService.add(card);
                        request.setAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(MESSAGE_ADD_CARD,
                                (String) session.getAttribute(LOCALE)));
                        return ConfigManager.getProperty(PAGE_USER);
                    } else {
                        request.setAttribute(ATTRIBUTE_MESSAGE, cardNumber + MessageManager.getProperty(MESSAGE_OCCUPIED_CARD,
                                (String) session.getAttribute(LOCALE)));
                        return ConfigManager.getProperty(PAGE_USER);
                    }
                } catch (ServiceException e) {
                    throw new ApplicationException(e);
                }
            } else {
                request.setAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(MESSAGE_VALID,
                        (String) session.getAttribute(LOCALE)));
                return ConfigManager.getProperty(PAGE_USER);
            }
        } else {
            throw new ApplicationException(MessageManager.getProperty(MESSAGE_ACCESS_ERROR,
                    (String) session.getAttribute(LOCALE)));
        }
    }
}
