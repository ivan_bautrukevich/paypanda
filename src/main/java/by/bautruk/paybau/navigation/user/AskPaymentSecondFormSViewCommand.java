package by.bautruk.paybau.navigation.user;

import by.bautruk.paybau.config.ConfigManager;
import by.bautruk.paybau.config.MessageManager;
import by.bautruk.paybau.entity.Card;
import by.bautruk.paybau.entity.PaymentAccount;
import by.bautruk.paybau.entity.PaymentType;
import by.bautruk.paybau.entity.User;
import by.bautruk.paybau.exception.ApplicationException;
import by.bautruk.paybau.exception.ServiceException;
import by.bautruk.paybau.navigation.ActionCommand;
import by.bautruk.paybau.service.CardService;
import by.bautruk.paybau.service.PaymentAccountService;
import by.bautruk.paybau.service.PaymentTypeService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         17.01.2016 20:50.
 *         The {@code AskPaymentFirstFormViewCommand} class represents an ability of manipulation
 *         with an instance of {@code HttpServletRequest}.
 */
public class AskPaymentSecondFormSViewCommand implements ActionCommand {

    private static final String ATTRIBUTE_USER = "user";
    private static final String ROLE = "user";
    private static final String PAGE_PAYMENT = "page.payment";
    private static final String ATTRIBUTE_PREVIOUS_COMMAND = "previousCommand";
    private static final String PREVIOUS_COMMAND = "login";
    private static final String SELECT_ACCOUNT = "selectAccount";
    private static final String ATTRIBUTE_TYPES = "types";
    private static final String ATTRIBUTE_CARD = "card";
    private static final String LOCALE = "locale";
    private static final String MESSAGE_ACCESS_ERROR = "message.accesserror";

    /**
     * Forwarded page with with form with user's card for creating
     * new payment.
     *
     * @param request is an instance of {@code HttpServletRequest}.
     * @return a result page at which should be forwarded to.
     * @throws ApplicationException if there are any troubles with PaymentAccountService
     *                              if user doesn't have access for this operation.
     */

    @Override
    public String execute(HttpServletRequest request) throws ApplicationException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(ATTRIBUTE_USER);
        session.setAttribute(ATTRIBUTE_PREVIOUS_COMMAND, PREVIOUS_COMMAND);
        if (ROLE.equals(user.getRole().getRole())) {
            try {
                String selectAccount = request.getParameter(SELECT_ACCOUNT);
                PaymentTypeService paymentTypeService = new PaymentTypeService();
                List<PaymentType> types;
                types = paymentTypeService.takeAll();
                request.setAttribute(ATTRIBUTE_TYPES, types);
                PaymentAccountService paymentAccountService = new PaymentAccountService();
                PaymentAccount paymentAccount;
                Long accountId;
                accountId = paymentAccountService.takeByNumber(Long.parseLong(selectAccount));
                paymentAccount = paymentAccountService.takeById(accountId);
                CardService cardService = new CardService();
                Card card;
                card = cardService.takeById(paymentAccount.getCard().getId());
                request.setAttribute(ATTRIBUTE_CARD, card);
                return ConfigManager.getProperty(PAGE_PAYMENT);
            } catch (ServiceException e) {
                throw new ApplicationException(e);
            }
        } else {
            throw new ApplicationException(MessageManager.getProperty(MESSAGE_ACCESS_ERROR,
                    (String) session.getAttribute(LOCALE)));
        }
    }
}
