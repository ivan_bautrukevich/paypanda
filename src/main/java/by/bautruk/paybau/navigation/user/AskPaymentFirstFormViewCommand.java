package by.bautruk.paybau.navigation.user;

import by.bautruk.paybau.config.ConfigManager;
import by.bautruk.paybau.config.MessageManager;
import by.bautruk.paybau.entity.PaymentAccount;
import by.bautruk.paybau.entity.User;
import by.bautruk.paybau.exception.ApplicationException;
import by.bautruk.paybau.exception.ServiceException;
import by.bautruk.paybau.navigation.ActionCommand;
import by.bautruk.paybau.service.PaymentAccountService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         25.01.2016 23:04.
 *         The {@code AskPaymentFirstFormViewCommand} class represents an ability of manipulation
 *         with an instance of {@code HttpServletRequest}.
 */
public class AskPaymentFirstFormViewCommand implements ActionCommand {

    private static final String ATTRIBUTE_USER = "user";
    private static final String ROLE = "user";
    private static final String PAGE_PAYMENT_FIRST = "page.paymentFirst";
    private static final String ATTRIBUTE_PREVIOUS_COMMAND = "previousCommand";
    private static final String PREVIOUS_COMMAND = "ask_form_payment_first";
    private static final String USER_ID = "userId";
    private static final String ATTRIBUTE_ACCOUNTS = "accounts";
    private static final long DEFAULT_UNBLOCKED_STATUS_ID = 1;
    private static final String LOCALE = "locale";
    private static final String MESSAGE_ACCESS_ERROR = "message.accesserror";

    /**
     * Forwarded page with with form with user's payment accounts for creating
     * new payment.
     *
     * @param request is an instance of {@code HttpServletRequest}.
     * @return a result page at which should be forwarded to.
     * @throws ApplicationException if there are any troubles with PaymentAccountService
     *                              if user doesn't have access for this operation.
     */

    @Override
    public String execute(HttpServletRequest request) throws ApplicationException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(ATTRIBUTE_USER);
        session.setAttribute(ATTRIBUTE_PREVIOUS_COMMAND, PREVIOUS_COMMAND);
        if (ROLE.equals(user.getRole().getRole())) {
            try {
                String userIdString = String.valueOf(session.getAttribute(USER_ID));
                Long userId = Long.parseLong(userIdString);
                PaymentAccountService paymentAccountService = new PaymentAccountService();
                List<PaymentAccount> accounts = paymentAccountService.takeByUserIdAndStatusId(userId,
                        DEFAULT_UNBLOCKED_STATUS_ID);
                request.setAttribute(ATTRIBUTE_ACCOUNTS, accounts);
                return ConfigManager.getProperty(PAGE_PAYMENT_FIRST);
            } catch (ServiceException e) {
                throw new ApplicationException(e);
            }
        } else {
            throw new ApplicationException(MessageManager.getProperty(MESSAGE_ACCESS_ERROR,
                    (String) session.getAttribute(LOCALE)));
        }
    }
}