package by.bautruk.paybau.navigation;

import by.bautruk.paybau.exception.ApplicationException;

import javax.servlet.http.HttpServletRequest;

/**
 * @author ivan.bautruvkevich
 *         10.01.2016 14:02.
 *         The {@code ActionCommand} interface enumerates all the necessary methods
 *         for navigation cammand.
 */
public interface ActionCommand {
    String execute(HttpServletRequest request) throws ApplicationException;
}
