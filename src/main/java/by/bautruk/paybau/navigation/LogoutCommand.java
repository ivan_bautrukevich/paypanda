package by.bautruk.paybau.navigation;

import by.bautruk.paybau.config.ConfigManager;

import javax.servlet.http.HttpServletRequest;

/**
 * @author ivan.bautruvkevich
 *         10.01.2016 14:29.
 *         The {@code LogoutCommand} class represents an ability of manipulation
 *         with an instance of {@code HttpServletRequest}.
 */
public class LogoutCommand implements ActionCommand {

    private static final String PAGE_INDEX = "page.index";

    /**
     * Invalidate session.
     *
     * @param request is an instance of {@code HttpServletRequest}.
     * @return a result page at which should be forwarded to.
     */

    @Override
    public String execute(HttpServletRequest request) {
        request.getSession().invalidate();
        return ConfigManager.getProperty(PAGE_INDEX);
    }
}
