package by.bautruk.paybau.navigation;

import by.bautruk.paybau.config.ConfigManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author ivan.bautruvkevich
 *         15.01.2016 20:27.
 *         The {@code LogoutCommand} class represents an ability of manipulation
 *         with an instance of {@code HttpServletRequest}.
 */
public class SignUpCommand implements ActionCommand {

    private static final String PAGE_SIGN_UP = "page.signup";
    private static final String ATTRIBUTE_PREVIOUS_COMMAND = "previousCommand";
    private static final String PREVIOUS_COMMAND = "signup";

    /**
     *  Gives a string value of the page on which user can see form
     *  for registration.
     *
     * @param request is an instance of {@code HttpServletRequest}.
     * @return a result page at which should be forwarded to.
     */

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.setAttribute(ATTRIBUTE_PREVIOUS_COMMAND, PREVIOUS_COMMAND);
        return ConfigManager.getProperty(PAGE_SIGN_UP);
    }
}
