package by.bautruk.paybau.navigation;

import by.bautruk.paybau.config.MessageManager;
import by.bautruk.paybau.exception.ApplicationException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author ivan.bautruvkevich
 *         10.01.2016 14:05.
 *         The {@code ActionFactory} class represents an ability of manipulation
 *         with an instance of {@code HttpServletRequest}.
 */
public class ActionFactory {

    private static final String COMMAND = "command";
    private static final String LOCALE = "locale";
    private static final String MESSAGE_WRONG_ACTION = "message.wrongaction";

    /**
     * Finds and creates command.
     *
     * @param request is an instance of {@code HttpServletRequest}.
     * @return instance of {@code ActionCommand}.
     * @throws ApplicationException if {@code request} is null or empty
     *                              if {@code request} not exist.
     */

    public ActionCommand defineCommand(HttpServletRequest request) throws ApplicationException {
        HttpSession session = request.getSession();
        ActionCommand current;
        String action = request.getParameter(COMMAND);
        if (action == null || action.isEmpty()) {
            throw new ApplicationException(MessageManager.getProperty(action + MESSAGE_WRONG_ACTION,
                    (String) session.getAttribute(LOCALE)));
        }
        try {
            CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
            current = currentEnum.getCommand();
            return current;
        } catch (IllegalArgumentException e) {
            throw new ApplicationException(MessageManager.getProperty(MESSAGE_WRONG_ACTION,
                    (String) session.getAttribute(LOCALE)));
        }
    }
}
