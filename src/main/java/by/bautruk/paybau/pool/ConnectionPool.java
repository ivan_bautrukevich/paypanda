package by.bautruk.paybau.pool;

import by.bautruk.paybau.exception.DAOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author ivan.bautruvkevich
 *         10.01.2016 13:25.
 */
public class ConnectionPool {

    final static Logger LOG = LogManager.getLogger(ConnectionPool.class);

    private static final String USER = "user";
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String USE_UNICODE = "useUnicode";
    private static final String UNICODE = "unicode";
    private static final String CHARACTER_ENCODING = "characterEncoding";
    private static final String ENCODING = "encoding";
    private static final String URL = "url";

    private static final int WAITING_TIMEOUT_SEC = 10;
    private static ResourceBundle configBundle = ResourceBundle.getBundle("database");
    private final int DEFAULT_POOL_SIZE = Integer.parseInt(configBundle.getString("defaultPoolSize"));
    private static Lock lock = new ReentrantLock();
    private static AtomicBoolean giveConnection = new AtomicBoolean(true);
    private static AtomicBoolean createInstance = new AtomicBoolean(true);
    private static ConnectionPool instance;
    private ArrayBlockingQueue<Connection> pool;


    private ConnectionPool() {
        init();
    }


    private void init() {
        pool = new ArrayBlockingQueue<>(DEFAULT_POOL_SIZE);
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            Properties properties = new Properties();
            properties.setProperty(USER, configBundle.getString(LOGIN));
            properties.setProperty(PASSWORD, configBundle.getString(PASSWORD));
            properties.setProperty(USE_UNICODE, configBundle.getString(UNICODE));
            properties.setProperty(CHARACTER_ENCODING, configBundle.getString(ENCODING));
            for (int i = 0; i <= DEFAULT_POOL_SIZE; i++) {
                Connection connection = DriverManager.getConnection(configBundle.getString(URL), properties);
                pool.offer(connection);
            }
        } catch (SQLException e) {
            throw new RuntimeException();
        }
    }

    public static ConnectionPool getInstance() {
        if (createInstance.get()) {
            try {
                lock.lock();
                if (instance == null) {
                    instance = new ConnectionPool();
                    createInstance.set(false);
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    public Connection getConnection() throws DAOException {
        Connection connection = null;
        if (giveConnection.get()) {
            try {
                connection = pool.poll(WAITING_TIMEOUT_SEC, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                throw new DAOException("Exception occurred during getting connection");
            }
        }
        return connection;
    }

    public void returnConnection(Connection connection) {
        if (connection != null) {
            pool.offer(connection);
        }
    }

    public void cleanUp() {
        giveConnection = new AtomicBoolean(false);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            LOG.error("InterruptedException occurred during connection pool cleaning");
        }
        Iterator<Connection> iterator = pool.iterator();
        while (iterator.hasNext()) {
            Connection connection = iterator.next();
            try {
                connection.close();
            } catch (SQLException e) {
                LOG.error("Exception occurred during connection pool cleaning");
            }
            iterator.remove();
        }
    }
}
