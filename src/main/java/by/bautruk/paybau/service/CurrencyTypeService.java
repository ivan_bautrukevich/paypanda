package by.bautruk.paybau.service;

import by.bautruk.paybau.dao.CurrencyTypeDAO;
import by.bautruk.paybau.entity.CurrencyType;
import by.bautruk.paybau.exception.DAOException;
import by.bautruk.paybau.exception.ServiceException;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         25.01.2016 12:48.
 */
public class CurrencyTypeService implements IService<CurrencyType> {

    private CurrencyTypeDAO currencyTypeDAO = new CurrencyTypeDAO();

    @Override
    public List<CurrencyType> takeAll() throws ServiceException {
        try {
            return currencyTypeDAO.takeAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public CurrencyType takeById(Long id) throws ServiceException {
        return null;
    }

    @Override
    public void add(CurrencyType currencyType) throws ServiceException {

    }

    @Override
    public void deleteById(Long id) throws ServiceException {

    }

    @Override
    public void updateById(Long id, CurrencyType currencyType) throws ServiceException {

    }

    public CurrencyType defineByName(String name) throws ServiceException {
        try {
            return currencyTypeDAO.defineByName(name);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
