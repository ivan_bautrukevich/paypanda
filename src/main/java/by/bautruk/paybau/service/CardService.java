package by.bautruk.paybau.service;

import by.bautruk.paybau.dao.CardDAO;
import by.bautruk.paybau.entity.Card;
import by.bautruk.paybau.exception.DAOException;
import by.bautruk.paybau.exception.ServiceException;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         18.01.2016 12:34.
 */
public class CardService implements IService<Card> {

    private CardDAO cardDAO = new CardDAO();

    @Override
    public List<Card> takeAll() throws ServiceException {
        return null;
    }

    @Override
    public Card takeById(Long id) throws ServiceException {
        try {
            return cardDAO.takeById(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void add(Card card) throws ServiceException {
        try {
            cardDAO.add(card);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteById(Long id) throws ServiceException {

    }

    @Override
    public void updateById(Long id, Card card) throws ServiceException {

    }

    public List<Card> takeAllByUserId(Long userId) throws ServiceException {
        try {
            return cardDAO.takeAllByUserId(userId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public Card takeByNumber(long number) throws ServiceException {
        try {
            return cardDAO.takeByNumber(number);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
