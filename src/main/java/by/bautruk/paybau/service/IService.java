package by.bautruk.paybau.service;

import by.bautruk.paybau.exception.ServiceException;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         17.01.2016 13:13.
 */
public interface IService<T> {

    List<T> takeAll() throws ServiceException;

    T takeById(Long id) throws ServiceException;

    void add(T t) throws ServiceException;

    void deleteById(Long id) throws ServiceException;

    void updateById(Long id, T t) throws ServiceException;

}
