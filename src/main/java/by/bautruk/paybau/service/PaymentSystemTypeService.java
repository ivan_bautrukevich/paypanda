package by.bautruk.paybau.service;

import by.bautruk.paybau.dao.PaymentSystemTypeDAO;
import by.bautruk.paybau.entity.PaymentSystemType;
import by.bautruk.paybau.exception.DAOException;
import by.bautruk.paybau.exception.ServiceException;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         23.01.2016 10:23.
 */
public class PaymentSystemTypeService implements IService<PaymentSystemType> {

    private PaymentSystemTypeDAO paymentSystemTypeDAO = new PaymentSystemTypeDAO();

    @Override
    public List<PaymentSystemType> takeAll() throws ServiceException {

        try {
            return paymentSystemTypeDAO.takeAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

    }

    @Override
    public PaymentSystemType takeById(Long id) throws ServiceException {
        return null;
    }

    @Override
    public void add(PaymentSystemType paymentSystemType) throws ServiceException {
    }

    @Override
    public void deleteById(Long id) throws ServiceException {

    }

    @Override
    public void updateById(Long id, PaymentSystemType paymentSystemType) throws ServiceException {

    }

    public PaymentSystemType defineByName(String selectPaymentSystemType) throws ServiceException {
        try {
            return paymentSystemTypeDAO.defineByName(selectPaymentSystemType);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
