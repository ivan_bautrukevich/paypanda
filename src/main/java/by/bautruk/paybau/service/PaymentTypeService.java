package by.bautruk.paybau.service;

import by.bautruk.paybau.dao.PaymentTypeDAO;
import by.bautruk.paybau.entity.PaymentType;
import by.bautruk.paybau.exception.DAOException;
import by.bautruk.paybau.exception.ServiceException;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         17.01.2016 22:24.
 */
public class PaymentTypeService implements IService<PaymentType> {

    private PaymentTypeDAO paymentTypeDAO = new PaymentTypeDAO();

    @Override
    public List<PaymentType> takeAll() throws ServiceException {
        try {
            return paymentTypeDAO.takeAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public PaymentType takeById(Long id) throws ServiceException {
        return null;
    }

    @Override
    public void add(PaymentType paymentType) throws ServiceException {
    }

    @Override
    public void deleteById(Long id) throws ServiceException {

    }

    @Override
    public void updateById(Long id, PaymentType paymentType) throws ServiceException {

    }

    public PaymentType takeByName(String type) throws ServiceException {
        try {
            return paymentTypeDAO.takeByName(type);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
