package by.bautruk.paybau.service;

import by.bautruk.paybau.dao.PaymentDAO;
import by.bautruk.paybau.entity.Payment;
import by.bautruk.paybau.exception.DAOException;
import by.bautruk.paybau.exception.ServiceException;

import java.sql.Date;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         17.01.2016 12:37.
 */
public class PaymentService implements IService<Payment> {

    private PaymentDAO paymentDAO = new PaymentDAO();

    @Override
    public List<Payment> takeAll() throws ServiceException {
        return null;
    }

    @Override
    public Payment takeById(Long id) throws ServiceException {
        return null;
    }

    @Override
    public void add(Payment payment) throws ServiceException {
        try {
            paymentDAO.add(payment);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteById(Long id) throws ServiceException {

    }

    @Override
    public void updateById(Long id, Payment payment) throws ServiceException {

    }

    public List<Payment> takeAllByUserId(Long userId) throws ServiceException {
        try {
            return paymentDAO.takeAllByUserId(userId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public List<Payment> takeAllByDate(Date date) throws ServiceException {
        try {
            return paymentDAO.takeAllByDate(date);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
