package by.bautruk.paybau.service;

import by.bautruk.paybau.dao.UserDAO;
import by.bautruk.paybau.encryption.EncryptionData;
import by.bautruk.paybau.entity.User;
import by.bautruk.paybau.exception.DAOException;
import by.bautruk.paybau.exception.ServiceException;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         12.01.2016 20:41.
 */
public class UserService implements IService<User> {

    private UserDAO userDAO = new UserDAO();

    public User defineUser(String enteredLogin, String enteredPassword) throws ServiceException {
        String login = EncryptionData.getMD5(enteredLogin);
        String password = EncryptionData.getMD5(enteredPassword);
        try {
            return userDAO.defineUser(login, password);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<User> takeAll() throws ServiceException {
        try {
            return userDAO.takeAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public User takeById(Long id) throws ServiceException {
        return null;
    }

    public void add(User user) throws ServiceException {
        String login = user.getLogin();
        String password = user.getPassword();
        String ecnrLogin = EncryptionData.getMD5(login);
        String encrPassword = EncryptionData.getMD5(password);
        user.setLogin(ecnrLogin);
        user.setPassword(encrPassword);
        try {
            userDAO.add(user);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteById(Long id) throws ServiceException {
        try {
            userDAO.deleteById(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void updateById(Long id, User user) throws ServiceException {

    }

    public Long defineByLogin(String enteredLogin) throws ServiceException {
        String login = EncryptionData.getMD5(enteredLogin);
        try {
            return userDAO.defineByLogin(login);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
