package by.bautruk.paybau.service;

import by.bautruk.paybau.dao.RoleDAO;
import by.bautruk.paybau.entity.Role;
import by.bautruk.paybau.exception.DAOException;
import by.bautruk.paybau.exception.ServiceException;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         02.02.2016 19:39.
 */
public class RoleService implements IService<Role> {

    RoleDAO roleDAO = new RoleDAO();

    @Override
    public List<Role> takeAll() throws ServiceException {
        try {
            return roleDAO.takeAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Role takeById(Long id) throws ServiceException {
        return null;
    }

    @Override
    public void add(Role role) throws ServiceException {

    }

    @Override
    public void deleteById(Long id) throws ServiceException {

    }

    @Override
    public void updateById(Long id, Role role) throws ServiceException {

    }

    public Role defineByName(String name) throws ServiceException {
        try {
            return roleDAO.defineByName(name);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
