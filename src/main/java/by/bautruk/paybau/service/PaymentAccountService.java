package by.bautruk.paybau.service;

import by.bautruk.paybau.dao.PaymentAccountDAO;
import by.bautruk.paybau.entity.PaymentAccount;
import by.bautruk.paybau.exception.DAOException;
import by.bautruk.paybau.exception.ServiceException;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         22.01.2016 14:25.
 */
public class PaymentAccountService implements IService<PaymentAccount> {

    private PaymentAccountDAO paymentAccountDAO = new PaymentAccountDAO();

    @Override
    public List<PaymentAccount> takeAll() throws ServiceException {
        return null;
    }

    @Override
    public PaymentAccount takeById(Long id) throws ServiceException {
        try {
            return paymentAccountDAO.takeById(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void add(PaymentAccount paymentAccount) throws ServiceException {
        try {
            paymentAccountDAO.add(paymentAccount);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteById(Long id) throws ServiceException {

    }

    @Override
    public void updateById(Long id, PaymentAccount paymentAccount) throws ServiceException {
        try {
            paymentAccountDAO.updateById(id, paymentAccount);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }


    public List<PaymentAccount> takeAllByUserId(Long userId) throws ServiceException {
        try {
            return paymentAccountDAO.takeAllByUserId(userId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public List<PaymentAccount> takeByUserIdAndStatusId(Long userId, Long statusId) throws ServiceException {
        try {
            return paymentAccountDAO.takeByUserIdAndStatusId(userId, statusId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public Long takeByNumber(Long number) throws ServiceException {
        try {
            return paymentAccountDAO.takeByNumber(number);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

    }

    public PaymentAccount takeByCardId(Long cardId) throws ServiceException {
        try {
            return paymentAccountDAO.takeByCardId(cardId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public List<PaymentAccount> takeAllByStatusId(Long statusId) throws ServiceException {
        try {
            return paymentAccountDAO.takeAllByStatusId(statusId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}

