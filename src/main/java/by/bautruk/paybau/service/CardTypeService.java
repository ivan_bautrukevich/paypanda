package by.bautruk.paybau.service;

import by.bautruk.paybau.dao.CardTypeDAO;
import by.bautruk.paybau.entity.CardType;
import by.bautruk.paybau.exception.DAOException;
import by.bautruk.paybau.exception.ServiceException;

import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         23.01.2016 10:59.
 */
public class CardTypeService implements IService<CardType> {

    private CardTypeDAO cardTypeDAO = new CardTypeDAO();

    @Override
    public List<CardType> takeAll() throws ServiceException {
        try {
            return cardTypeDAO.takeAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public CardType takeById(Long id) throws ServiceException {
        return null;
    }

    @Override
    public void add(CardType cardType) throws ServiceException {
    }

    @Override
    public void deleteById(Long id) throws ServiceException {

    }

    @Override
    public void updateById(Long id, CardType cardType) throws ServiceException {

    }

    public CardType defineByType(String selectCardType) throws ServiceException {
        try {
            return cardTypeDAO.defineByType(selectCardType);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
