package by.bautruk.paybau.encryption;

import by.bautruk.paybau.exception.ServiceException;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author ivan.bautruvkevich
 *         15.01.2016 22:25.
 *         The {@code EncryptionData} class represents an ability of encrypt
 *         data.
 */
public class EncryptionData {

    private static final String MD5 = "MD5";

    /**
     * Encrypt data.
     *
     * @param input is String parameter which will be encrypted.
     * @return encrypted String.
     */

    public static String getMD5(String input) throws ServiceException {
        try {
            MessageDigest md = MessageDigest.getInstance(MD5);
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashText = number.toString(16);
            while (hashText.length() < 32) {
                hashText = "0" + hashText;
            }
            return hashText;
        } catch (NoSuchAlgorithmException e) {
            throw new ServiceException("Wrong parameter for encryption");
        }
    }
}
