package by.bautruk.paybau.dao;

import by.bautruk.paybau.exception.DAOException;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         17.01.2016 13:04.
 *         The {@code IDao} interface enumerates all the necessary methods
 *         for manipulation with data in database.
 */
public interface IDao<T> {

    List<T> takeAll() throws DAOException;

    T takeById(Long id) throws DAOException;

    void add(T t) throws DAOException;

    void deleteById(Long id) throws DAOException;

    void updateById(Long id, T t) throws DAOException;


    /**
     * Default method for closing Statement connection.
     *
     * @param statement is an instance of {@code Statement}
     *                  class.
     */
    default void close(Statement statement) throws DAOException {
        try {
            if (statement != null) {
                statement.close();
            }
        } catch (SQLException e) {
            throw new DAOException("Exception occurred during closing statement", e);
        }
    }

}
