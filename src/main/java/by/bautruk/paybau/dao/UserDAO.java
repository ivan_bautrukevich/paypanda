package by.bautruk.paybau.dao;

import by.bautruk.paybau.entity.Role;
import by.bautruk.paybau.entity.User;
import by.bautruk.paybau.exception.DAOException;
import by.bautruk.paybau.pool.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         10.01.2016 15:13.
 *         The {@code UserDAO} class implements all the necessary methods
 *         which implements {@code IDAO} interface for manipulation
 *         with user's data in database.
 */
public class UserDAO implements IDao<User> {

    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String USER_ID = "user_id";
    private static final String EMAIL = "email";
    private static final String ROLE = "role";


    private static final String SQL_FIND_ALL = "SELECT user.user_id, user.first_name, user.last_name, user.email, role.role " +
            "FROM paypanda_system.user " +
            "JOIN paypanda_system.role ON user.role_id=role.role_id";

    private static final String SQL_ADD_USER = "INSERT INTO paypanda_system.user (first_name, last_name," +
            "email, login, password, role_id) VALUES (?, ?, ?, ?, ?, ?)";

    private static final String SQL_DELETE_USER = "DELETE FROM paypanda_system.user WHERE `user_id`=?;";

    private static final String SQL_CHECK_LOGIN = "SELECT user.user_id FROM paypanda_system.user " +
            "WHERE login=?";

    private static final String SQL_FIND_USER = "SELECT user.user_id, user.first_name, user.last_name, user.email, user.login, " +
            "user.password, role.role FROM paypanda_system.user  JOIN paypanda_system.role ON user.role_id=role.role_id " +
            "WHERE login=? AND password=?";


    /**
     * Adds definite user to database.
     *
     * @param user is an instance of {@code User} class that
     *             has to be placed into table.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public void add(User user) throws DAOException {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_ADD_USER);
            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getLogin());
            preparedStatement.setString(5, user.getPassword());
            preparedStatement.setLong(6, user.getRole().getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during user adding", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }
    }

    /**
     * Delete definite user from database.
     *
     * @param id of user.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public void deleteById(Long id) throws DAOException {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_DELETE_USER);
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during user deleting", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }
    }

    @Override
    public void updateById(Long id, User user) throws DAOException {

    }

    /**
     * Gives the list of users from database.
     *
     * @return the list of users.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<User> takeAll() throws DAOException {
        List<User> users = new ArrayList<>();
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_ALL);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                User user = new User();
                user.setFirstName(resultSet.getString(FIRST_NAME));
                user.setLastName(resultSet.getString(LAST_NAME));
                user.setEmail(resultSet.getString(EMAIL));
                Role role = new Role();
                role.setRole(resultSet.getString(ROLE));
                user.setRole(role);
                Long userId = Long.valueOf(resultSet.getString(USER_ID));
                user.setId(userId);
                users.add(user);
            }
            return users;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during user getting", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }
    }

    @Override
    public User takeById(Long id) throws DAOException {
        return null;
    }

    /**
     * Represents the way of getting user as an instance of
     * {@code User} through {@code login} and {@code password} values.
     *
     * @param login    of user.
     * @param password of user.
     * @return the only instance of {@code User} that defined just by using
     * {@code login} and {@code password} values.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    public User defineUser(String login, String password) throws DAOException {
        User user = new User();
        Role role = new Role();
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_USER);
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user.setFirstName(resultSet.getString(FIRST_NAME));
                user.setLastName(resultSet.getString(LAST_NAME));
                user.setId(Long.parseLong(resultSet.getString(USER_ID)));
                user.setEmail(resultSet.getString(EMAIL));
                role.setRole(resultSet.getString(ROLE));
                user.setRole(role);
            }
            return user;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during user", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }
    }

    /**
     * Represents the way of getting user ID  through {@code login} value.
     *
     * @param login of user.
     * @return user's ID.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    public Long defineByLogin(String login) throws DAOException {
        Long userId = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_CHECK_LOGIN);
            preparedStatement.setString(1, login);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                userId = resultSet.getLong(USER_ID);
            }
            return userId;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during user id", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }
    }
}
