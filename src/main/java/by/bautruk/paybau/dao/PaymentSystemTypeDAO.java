package by.bautruk.paybau.dao;

import by.bautruk.paybau.entity.PaymentSystemType;
import by.bautruk.paybau.exception.DAOException;
import by.bautruk.paybau.pool.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         23.01.2016 10:44.
 *         The {@code PaymentSystemTypeDAO} class implements all the necessary methods
 *         which implements {@code IDAO} interface for manipulation
 *         with payment system type's data in database.
 */
public class PaymentSystemTypeDAO implements IDao<PaymentSystemType> {

    private static final String PAYMENTS_SYSTEM_TYPE = "payment_system_type";
    private static final String PAYMENTS_SYSTEM_TYPE_ID = "payment_system_type_id";

    private static final String SQL_FIND_ALL = "SELECT * FROM paypanda_system.payment_system_type";
    private static final String SQL_FIND_BY_NAME = "SELECT * FROM paypanda_system.payment_system_type " +
            "WHERE payment_system_type.payment_system_type = ?";

    /**
     * Gives the list of payment system types from database.
     *
     * @return the list of payment system types.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<PaymentSystemType> takeAll() throws DAOException {
        List<PaymentSystemType> paymentSystemTypes = new ArrayList<>();
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_ALL);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                PaymentSystemType paymentSystemType = new PaymentSystemType();
                paymentSystemType.setType(resultSet.getString(PAYMENTS_SYSTEM_TYPE));
                paymentSystemTypes.add(paymentSystemType);
            }
            return paymentSystemTypes;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during payment system type getting", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }
    }

    @Override
    public PaymentSystemType takeById(Long id) throws DAOException {
        return null;
    }

    @Override
    public void add(PaymentSystemType paymentSystemType) throws DAOException {
    }

    @Override
    public void deleteById(Long id) throws DAOException {

    }

    @Override
    public void updateById(Long id, PaymentSystemType paymentSystemType) throws DAOException {

    }

    /**
     * Represents the way of getting payment system type as an instance of
     * {@code PaymentSystemType} through {@code selectPaymentSystemType} value.
     *
     * @param selectPaymentSystemType is name of type.
     * @return the only instance of {@code PaymentSystemType} that defined just by using
     * {@code selectPaymentSystemType}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    public PaymentSystemType defineByName(String selectPaymentSystemType) throws DAOException {
        PaymentSystemType paymentSystemType = new PaymentSystemType();
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_BY_NAME);
            preparedStatement.setString(1, selectPaymentSystemType);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                String id = resultSet.getString(PAYMENTS_SYSTEM_TYPE_ID);
                paymentSystemType.setId(Long.parseLong(id));
                paymentSystemType.setType(resultSet.getString(PAYMENTS_SYSTEM_TYPE));
            }
            return paymentSystemType;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during payment system type getting", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }
    }
}

