package by.bautruk.paybau.dao;

import by.bautruk.paybau.entity.Card;
import by.bautruk.paybau.entity.CurrencyType;
import by.bautruk.paybau.entity.PaymentAccount;
import by.bautruk.paybau.entity.PaymentAccountStatus;
import by.bautruk.paybau.exception.DAOException;
import by.bautruk.paybau.pool.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         22.01.2016 14:28.
 *         The {@code PaymentAccountDAO} class implements all the necessary methods
 *         which implements {@code IDAO} interface for manipulation
 *         with card's data in database.
 */
public class PaymentAccountDAO implements IDao<PaymentAccount> {

    private static final String BALANCE = "balance";
    private static final String PAYMENT_ACCOUNT_ID = "payment_account_id";
    private static final String PAYMENT_ACCOUNT_NUMBER = "payment_account_number";
    private static final String CURRENCY = "currency";
    private static final String STATUS_ID = "status_id";
    private static final String STATUS = "status";
    private static final String CARD_ID = "card_id";
    private static final String CARD_NUMBER = "card_number";

    private static final String SQL_FIND_ACCOUNT_BY_ID = "SELECT payment_account.payment_account_number, payment_account.balance," +
            "payment_account.card_id ,currency.currency, payment_account.status_id, status.status " +
            "FROM paypanda_system.payment_account " +
            "JOIN paypanda_system.status ON payment_account.status_id = status.status_id " +
            "JOIN paypanda_system.card ON payment_account.card_id = card.card_id " +
            "JOIN paypanda_system.currency ON card.code_currency = currency.code_currency " +
            "WHERE payment_account.payment_account_id = ?";

    private static final String SQL_FIND_ACCOUNT_BY_CARD_ID = "SELECT payment_account.payment_account_id, " +
            "payment_account.payment_account_number, payment_account.balance," +
            "payment_account.card_id ,currency.currency, payment_account.status_id, status.status " +
            "FROM paypanda_system.payment_account " +
            "JOIN paypanda_system.status ON payment_account.status_id = status.status_id " +
            "JOIN paypanda_system.card ON payment_account.card_id = card.card_id " +
            "JOIN paypanda_system.currency ON card.code_currency = currency.code_currency " +
            "WHERE payment_account.card_id = ?";

    private static final String SQL_ADD_ACCOUNT = "INSERT INTO paypanda_system.payment_account(payment_account_number, card_id, " +
            "balance, status_id) " +
            "VALUES (?, ?, ?, ?)";

    private static final String SQL_FIND_ACCOUNT_BY_NUMBER = "SELECT payment_account.payment_account_id " +
            "FROM paypanda_system.payment_account " +
            "WHERE payment_account.payment_account_number = ?";

    private static final String SQL_FIND_ACCOUNT_BY_USER_ID = "SELECT payment_account.payment_account_number, payment_account.balance, " +
            "status.status, currency.currency, card.card_number " +
            "FROM paypanda_system.payment_account " +
            "JOIN paypanda_system.status ON payment_account.status_id = status.status_id " +
            "JOIN paypanda_system.card ON payment_account.card_id = card.card_id " +
            "JOIN paypanda_system.currency ON card.code_currency = currency.code_currency " +
            "JOIN paypanda_system.user ON card.user_id = user.user_id " +
            "WHERE user.user_id =? ORDER BY payment_account.payment_account_number";

    private static final String SQL_FIND_ACCOUNT_BY_USER_ID_AND_STATUS_ID = "SELECT payment_account.payment_account_number, " +
            "payment_account.balance, status.status, currency.currency, card.card_number " +
            "FROM paypanda_system.payment_account " +
            "JOIN paypanda_system.status ON payment_account.status_id = status.status_id " +
            "JOIN paypanda_system.card ON payment_account.card_id = card.card_id " +
            "JOIN paypanda_system.currency ON card.code_currency = currency.code_currency " +
            "JOIN paypanda_system.user ON card.user_id = user.user_id " +
            "WHERE user.user_id =? AND payment_account.status_id=? ORDER BY payment_account.payment_account_number";

    private static final String SQL_FIND_ACCOUNT_BY_STATUS_ID = "SELECT payment_account.payment_account_number, " +
            "payment_account.balance, status.status, currency.currency, card.card_number " +
            "FROM paypanda_system.payment_account " +
            "JOIN paypanda_system.status ON payment_account.status_id = status.status_id " +
            "JOIN paypanda_system.card ON payment_account.card_id = card.card_id " +
            "JOIN paypanda_system.currency ON card.code_currency = currency.code_currency " +
            "WHERE payment_account.status_id=? ORDER BY payment_account.payment_account_number";

    private static final String SQL_UPDATE_BY_ID = "UPDATE paypanda_system.payment_account " +
            "SET payment_account_number=?, card_id=?, balance=?, status_id=? WHERE payment_account_id=?";


    @Override
    public List<PaymentAccount> takeAll() throws DAOException {
        return null;
    }

    /**
     * Gives the list of payment account by {@code id} from database.
     *
     * @param id of payment account.
     * @return the only instance of {@code PaymentAccount} that defined just by using
     * {@code id} value.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public PaymentAccount takeById(Long id) throws DAOException {
        PaymentAccount paymentAccount = new PaymentAccount();
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_ACCOUNT_BY_ID);
            preparedStatement.setLong(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                float balance = resultSet.getFloat(BALANCE);
                Long number = resultSet.getLong(PAYMENT_ACCOUNT_NUMBER);
                String typeName = resultSet.getString(CURRENCY);
                String statusId = resultSet.getString(STATUS_ID);
                String status = resultSet.getString(STATUS);
                String cardId = resultSet.getString(CARD_ID);
                CurrencyType type = new CurrencyType();
                type.setName(typeName);
                Card card = new Card();
                card.setId(Long.parseLong(cardId));
                paymentAccount.setId(id);
                paymentAccount.setBalance(balance);
                paymentAccount.setNumber(number);
                PaymentAccountStatus paymentAccountStatus = new PaymentAccountStatus();
                paymentAccountStatus.setStatus(status);
                paymentAccountStatus.setId(Long.parseLong(statusId));
                paymentAccount.setPaymentAccountStatus(paymentAccountStatus);
                paymentAccount.setCard(card);
            }
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during payment account taking", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }
        return paymentAccount;
    }

    /**
     * Adds definite payment account to database.
     *
     * @param paymentAccount is an instance of {@code PaymentAccount} class that
     *                       has to be placed into table.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public void add(PaymentAccount paymentAccount) throws DAOException {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_ADD_ACCOUNT);

            Long number = paymentAccount.getNumber();
            preparedStatement.setLong(1, number);

            Long cardId = paymentAccount.getCard().getId();
            preparedStatement.setLong(2, cardId);

            preparedStatement.setLong(3, 0);
            preparedStatement.setLong(4, 1);


            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during user adding", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }
    }

    @Override
    public void deleteById(Long id) throws DAOException {

    }

    /**
     * Updates definite payment account to database.
     *
     * @param paymentAccount is an instance of {@code PaymentAccount} class that
     *                       has to be placed into table.
     * @param id             is id of definite {@code paymentAccount}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public void updateById(Long id, PaymentAccount paymentAccount) throws DAOException {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_UPDATE_BY_ID);
            preparedStatement.setLong(1, paymentAccount.getNumber());
            preparedStatement.setLong(2, paymentAccount.getCard().getId());
            preparedStatement.setFloat(3, paymentAccount.getBalance());
            preparedStatement.setLong(4, paymentAccount.getPaymentAccountStatus().getId());
            preparedStatement.setLong(5, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during account taking", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }

    }

    /**
     * Gives the list of payment account by {@code userId} from database.
     *
     * @param userId is ID selected user.
     * @return the list of payment account.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    public List<PaymentAccount> takeAllByUserId(Long userId) throws DAOException {
        List<PaymentAccount> paymentAccounts = new ArrayList<>();
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_ACCOUNT_BY_USER_ID);
            preparedStatement.setLong(1, userId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                PaymentAccount paymentAccount = new PaymentAccount();
                float balance = resultSet.getFloat(BALANCE);
                Long number = resultSet.getLong(PAYMENT_ACCOUNT_NUMBER);
                String currency = resultSet.getString(CURRENCY);
                Card card = new Card();
                CurrencyType currencyType = new CurrencyType();
                currencyType.setName(currency);
                Long cardNumber = resultSet.getLong(CARD_NUMBER);
                card.setCurrencyType(currencyType);
                card.setNumber(cardNumber);
                String status = resultSet.getString(STATUS);
                PaymentAccountStatus paymentAccountStatus = new PaymentAccountStatus();
                paymentAccountStatus.setStatus(status);
                paymentAccount.setPaymentAccountStatus(paymentAccountStatus);
                paymentAccount.setCard(card);
                paymentAccount.setNumber(number);
                paymentAccount.setBalance(balance);
                paymentAccounts.add(paymentAccount);
            }
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during payment account taking", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }
        return paymentAccounts;
    }

    /**
     * Represents the way of getting payment account's ID  through {@code number} value.
     *
     * @param number of payment account.
     * @return payment account's ID.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    public Long takeByNumber(Long number) throws DAOException {
        Long id = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_ACCOUNT_BY_NUMBER);
            preparedStatement.setLong(1, number);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                id = resultSet.getLong(PAYMENT_ACCOUNT_ID);
            }
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during id taking", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }
        return id;
    }

    /**
     * Represents the way of getting user as an instance of
     * {@code PaymentAccount} through {@code cardId} value.
     *
     * @param cardId id ID of card.
     * @return the only instance of {@code PaymentAccount} that defined just by using
     * {@code cardId}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    public PaymentAccount takeByCardId(Long cardId) throws DAOException {
        PaymentAccount paymentAccount = new PaymentAccount();
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_ACCOUNT_BY_CARD_ID);
            preparedStatement.setLong(1, cardId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                float balance = resultSet.getFloat(BALANCE);
                Long number = resultSet.getLong(PAYMENT_ACCOUNT_NUMBER);
                String typeName = resultSet.getString(CURRENCY);
                String statusId = resultSet.getString(STATUS_ID);
                String status = resultSet.getString(STATUS);
                String id = resultSet.getString(PAYMENT_ACCOUNT_ID);
                CurrencyType type = new CurrencyType();
                type.setName(typeName);
                Card card = new Card();
                card.setId(cardId);
                paymentAccount.setId(Long.parseLong(id));
                paymentAccount.setBalance(balance);
                paymentAccount.setNumber(number);
                PaymentAccountStatus paymentAccountStatus = new PaymentAccountStatus();
                paymentAccountStatus.setStatus(status);
                paymentAccountStatus.setId(Long.parseLong(statusId));
                paymentAccount.setPaymentAccountStatus(paymentAccountStatus);
                paymentAccount.setCard(card);
            }
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during payment account taking", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }
        return paymentAccount;
    }

    /**
     * Gives the list of payment account by {@code userId} and
     * {@code statusId}from database.
     *
     * @param userId   is ID selected user.
     * @param statusId is ID of status of payment account.
     * @return the list of payment account.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    public List<PaymentAccount> takeByUserIdAndStatusId(Long userId, Long statusId) throws DAOException {
        List<PaymentAccount> paymentAccounts = new ArrayList<>();
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_ACCOUNT_BY_USER_ID_AND_STATUS_ID);
            preparedStatement.setLong(1, userId);
            preparedStatement.setLong(2, statusId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                PaymentAccount paymentAccount = new PaymentAccount();
                float balance = resultSet.getFloat(BALANCE);
                Long number = resultSet.getLong(PAYMENT_ACCOUNT_NUMBER);
                String currency = resultSet.getString(CURRENCY);
                Card card = new Card();
                CurrencyType currencyType = new CurrencyType();
                currencyType.setName(currency);
                Long cardNumber = resultSet.getLong(CARD_NUMBER);
                card.setCurrencyType(currencyType);
                card.setNumber(cardNumber);
                String status = resultSet.getString(STATUS);
                PaymentAccountStatus paymentAccountStatus = new PaymentAccountStatus();
                paymentAccountStatus.setStatus(status);
                paymentAccount.setPaymentAccountStatus(paymentAccountStatus);
                paymentAccount.setCard(card);
                paymentAccount.setNumber(number);
                paymentAccount.setBalance(balance);
                paymentAccounts.add(paymentAccount);
            }
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during payment accounts taking", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }
        return paymentAccounts;
    }

    /**
     * Gives the list of payment account by {@code statusId}from database.
     *
     * @param statusId is ID of status of payment account.
     * @return the list of payment account.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    public List<PaymentAccount> takeAllByStatusId(Long statusId) throws DAOException {
        List<PaymentAccount> paymentAccounts = new ArrayList<>();
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_ACCOUNT_BY_STATUS_ID);
            preparedStatement.setLong(1, statusId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                PaymentAccount paymentAccount = new PaymentAccount();
                float balance = resultSet.getFloat(BALANCE);
                Long number = resultSet.getLong(PAYMENT_ACCOUNT_NUMBER);
                String currency = resultSet.getString(CURRENCY);
                Card card = new Card();
                CurrencyType currencyType = new CurrencyType();
                currencyType.setName(currency);
                Long cardNumber = resultSet.getLong(CARD_NUMBER);
                card.setCurrencyType(currencyType);
                card.setNumber(cardNumber);
                String status = resultSet.getString(STATUS);
                PaymentAccountStatus paymentAccountStatus = new PaymentAccountStatus();
                paymentAccountStatus.setStatus(status);
                paymentAccount.setPaymentAccountStatus(paymentAccountStatus);
                paymentAccount.setCard(card);
                paymentAccount.setNumber(number);
                paymentAccount.setBalance(balance);
                paymentAccounts.add(paymentAccount);
            }
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during payment accounts taking", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }
        return paymentAccounts;
    }
}
