package by.bautruk.paybau.dao;

import by.bautruk.paybau.entity.PaymentType;
import by.bautruk.paybau.exception.DAOException;
import by.bautruk.paybau.pool.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         17.01.2016 22:29.
 *         The {@code PaymentTypeDAO} class implements all the necessary methods
 *         which implements {@code IDAO} interface for manipulation
 *         with card's data in database.
 */
public class PaymentTypeDAO implements IDao<PaymentType> {

    private static final String PAYMENT_TYPE = "payment_type";
    private static final String PAYMENT_TYPE_ID = "payment_type_id";

    private static final String SQL_FIND_ALL = "SELECT * FROM paypanda_system.payment_type";
    private static final String SQL_FIND_BY_NAME = "SELECT * FROM paypanda_system.payment_type WHERE payment_type.payment_type = ?";

    /**
     * Gives the list of payment types from database.
     *
     * @return the list of payment types.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<PaymentType> takeAll() throws DAOException {
        List<PaymentType> paymentTypes = new ArrayList<>();
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_ALL);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                PaymentType paymentType = new PaymentType();
                paymentType.setType(resultSet.getString(PAYMENT_TYPE));
                paymentTypes.add(paymentType);
            }
            return paymentTypes;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during payment type getting", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }
    }

    @Override
    public PaymentType takeById(Long id) throws DAOException {
        return null;
    }

    @Override
    public void add(PaymentType paymentType) throws DAOException {
    }

    @Override
    public void deleteById(Long id) throws DAOException {

    }

    @Override
    public void updateById(Long id, PaymentType paymentType) throws DAOException {

    }

    /**
     * Represents the way of getting payment type as an instance of
     * {@code PaymentType} through {@code type} value.
     *
     * @param type is name of payment type.
     * @return the only instance of {@code CardType} that defined just by using
     * {@code type}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    public PaymentType takeByName(String type) throws DAOException {
        PaymentType paymentType = new PaymentType();
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_BY_NAME);
            preparedStatement.setString(1, type);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                paymentType.setId(resultSet.getLong(PAYMENT_TYPE_ID));
                paymentType.setType(resultSet.getString(PAYMENT_TYPE));
            }
            return paymentType;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during payment type getting", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }
    }
}
