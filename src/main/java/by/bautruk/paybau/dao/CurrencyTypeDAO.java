package by.bautruk.paybau.dao;

import by.bautruk.paybau.entity.CurrencyType;
import by.bautruk.paybau.exception.DAOException;
import by.bautruk.paybau.pool.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         25.01.2016 12:53.
 *         The {@code CurrencyTypeDAO} class implements all the necessary methods
 *         which implements {@code IDAO} interface for manipulation
 *         with currency type in database.
 */
public class CurrencyTypeDAO implements IDao<CurrencyType> {

    private static final String CURRENCY = "currency";
    private static final String CODE_CURRENCY = "code_currency";

    private static final String SQL_FIND_ALL = "SELECT * FROM paypanda_system.currency";
    private static final String SQL_FIND_BY_NAME = "SELECT * FROM paypanda_system.currency WHERE currency.currency = ?";

    /**
     * Gives the list of currency type from database.
     *
     * @return the list of currency type.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<CurrencyType> takeAll() throws DAOException {
        List<CurrencyType> currencyTypes = new ArrayList<>();
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_ALL);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                CurrencyType currencyType = new CurrencyType();
                currencyType.setName(resultSet.getString(CURRENCY));
                currencyTypes.add(currencyType);
            }
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during currency type getting", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }
        return currencyTypes;
    }

    @Override
    public CurrencyType takeById(Long id) throws DAOException {
        return null;
    }

    @Override
    public void add(CurrencyType currencyType) throws DAOException {

    }

    @Override
    public void deleteById(Long id) throws DAOException {

    }

    @Override
    public void updateById(Long id, CurrencyType currencyType) throws DAOException {

    }

    /**
     * Represents the way of getting card type as an instance of
     * {@code CurrencyType} through {@code name} value.
     *
     * @param name is name of currency.
     * @return the only instance of {@code CardType} that defined just by using
     * {@code selectCardType}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    public CurrencyType defineByName(String name) throws DAOException {
        CurrencyType currencyType = new CurrencyType();
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_BY_NAME);
            preparedStatement.setString(1, name);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                currencyType.setId(Long.parseLong(resultSet.getString(CODE_CURRENCY)));
                currencyType.setName(resultSet.getString(CURRENCY));
            }
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during currency type getting", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }
        return currencyType;
    }
}
