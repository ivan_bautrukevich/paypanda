package by.bautruk.paybau.dao;

import by.bautruk.paybau.entity.Role;
import by.bautruk.paybau.exception.DAOException;
import by.bautruk.paybau.pool.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         03.02.2016 9:28.
 *         The {@code RoleDAO} class implements all the necessary methods
 *         which implements {@code IDAO} interface for manipulation
 *         with role's data in database.
 */
public class RoleDAO implements IDao<Role> {

    private static final String ROLE = "role";
    private static final String ROLE_ID = "role_id";

    private static final String SQL_FIND_ALL = "SELECT * FROM paypanda_system.role";
    private static final String SQL_FIND_BY_NAME = "SELECT * FROM paypanda_system.role WHERE role.role = ?";

    /**
     * Gives the list of roles from database.
     *
     * @return the list of roles.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<Role> takeAll() throws DAOException {
        List<Role> roles = new ArrayList<>();
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_ALL);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Role role = new Role();
                role.setRole(resultSet.getString(ROLE));
                roles.add(role);
            }
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during roles getting", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }
        return roles;
    }

    @Override
    public Role takeById(Long id) throws DAOException {
        return null;
    }

    @Override
    public void add(Role role) throws DAOException {

    }

    @Override
    public void deleteById(Long id) throws DAOException {

    }

    @Override
    public void updateById(Long id, Role role) throws DAOException {

    }

    /**
     * Represents the way of getting role as an instance of
     * {@code Role} through {@code name} value.
     *
     * @param name is name of role.
     * @return the only instance of {@code Role} that defined just by using
     * {@code name}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    public Role defineByName(String name) throws DAOException {
        Role role = new Role();
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_BY_NAME);
            preparedStatement.setString(1, name);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                role.setId(Long.parseLong(resultSet.getString(ROLE_ID)));
                role.setRole(resultSet.getString(ROLE));
            }
            return role;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during role getting", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }
    }
}
