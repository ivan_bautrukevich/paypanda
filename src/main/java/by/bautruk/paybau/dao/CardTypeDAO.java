package by.bautruk.paybau.dao;

import by.bautruk.paybau.entity.CardType;
import by.bautruk.paybau.exception.DAOException;
import by.bautruk.paybau.pool.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         23.01.2016 11:03.
 *         The {@code CardTypeDAO} class implements all the necessary methods
 *         which implements {@code IDAO} interface for manipulation
 *         with card type data in database.
 */
public class CardTypeDAO implements IDao<CardType> {

    private static final String CARD_TYPE = "card_type";
    private static final String CARD_TYPE_ID = "card_type_id";

    private static final String SQL_FIND_ALL = "SELECT * FROM paypanda_system.card_type";
    private static final String SQL_FIND_BY_TYPE = "SELECT * FROM paypanda_system.card_type WHERE card_type.card_type = ?";

    /**
     * Gives the list of card type from database.
     *
     * @return the list of card type.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public List<CardType> takeAll() throws DAOException {
        List<CardType> cardTypes = new ArrayList<>();
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_ALL);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                CardType cardType = new CardType();
                cardType.setType(resultSet.getString(CARD_TYPE));
                cardTypes.add(cardType);
            }
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during card type getting", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }
        return cardTypes;
    }

    @Override
    public CardType takeById(Long id) throws DAOException {
        return null;
    }

    @Override
    public void add(CardType cardType) throws DAOException {
    }

    @Override
    public void deleteById(Long id) throws DAOException {

    }

    @Override
    public void updateById(Long id, CardType cardType) throws DAOException {

    }

    /**
     * Represents the way of getting card type as an instance of
     * {@code CardType} through {@code selectCardType} value.
     *
     * @param selectCardType is name of type.
     * @return the only instance of {@code CardType} that defined just by using
     * {@code selectCardType}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    public CardType defineByType(String selectCardType) throws DAOException {
        CardType cardType = new CardType();
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_BY_TYPE);
            preparedStatement.setString(1, selectCardType);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                cardType.setId(Long.parseLong(resultSet.getString(CARD_TYPE_ID)));
                cardType.setType(resultSet.getString(CARD_TYPE));
            }
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during card type getting", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }
        return cardType;
    }
}
