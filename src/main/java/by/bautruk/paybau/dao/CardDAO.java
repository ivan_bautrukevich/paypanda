package by.bautruk.paybau.dao;

import by.bautruk.paybau.entity.*;
import by.bautruk.paybau.exception.DAOException;
import by.bautruk.paybau.pool.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         18.01.2016 12:38.
 *         The {@code CardDAO} class implements all the necessary methods
 *         which implements {@code IDAO} interface for manipulation
 *         with card's data in database.
 */
public class CardDAO implements IDao<Card> {

    private static final String CARD_ID = "card_id";
    private static final String CARD_NUMBER = "card_number";
    private static final String CARD_TYPE = "card_type";
    private static final String PAYMENT_SYSTEM_TYPE = "payment_system_type";
    private static final String CVV = "cvv";
    private static final String FIRST_NAME_HOLDER = "first_name_holder";
    private static final String LAST_NAME_HOLDER = "last_name_holder";
    private static final String USER_ID = "user_id";
    private static final String CURRENCY = "currency";

    private static final String SQL_ADD_CARD = "INSERT INTO paypanda_system.card(card_number, user_id, card_type_id, " +
            "payment_system_type_id, cvv, first_name_holder, last_name_holder, code_currency) " +
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

    private static final String SQL_FIND_CARD_BY_USER_ID = "SELECT card.card_number, card.first_name_holder, card.last_name_holder, " +
            "card_type.card_type, payment_system_type.payment_system_type, currency.currency " +
            "FROM paypanda_system.card " +
            "JOIN paypanda_system.currency ON card.code_currency = currency.code_currency " +
            "JOIN paypanda_system.card_type ON card.card_type_id = card_type.card_type_id " +
            "JOIN paypanda_system.payment_system_type ON card.payment_system_type_id = payment_system_type.payment_system_type_id " +
            "JOIN paypanda_system.user ON card.user_id = user.user_id " +
            "WHERE user.user_id =? ORDER BY card.card_number";

    private static final String SQL_FIND_CARD_BY_NUMBER = "SELECT card.card_id, card.card_number, card.user_id, card.first_name_holder, " +
            "card.last_name_holder, card.cvv, card_type.card_type, payment_system_type.payment_system_type, currency.currency " +
            "FROM paypanda_system.card  " +
            "JOIN paypanda_system.card_type ON card.card_type_id = card_type.card_type_id " +
            "JOIN paypanda_system.payment_system_type ON card.payment_system_type_id = payment_system_type.payment_system_type_id " +
            "JOIN paypanda_system.currency ON card.code_currency = currency.code_currency " +
            "WHERE card.card_number = ?";

    private static final String SQL_FIND_CARD_BY_ID = "SELECT card.card_id, card.card_number, card.user_id, card.first_name_holder, " +
            "card.last_name_holder, card.cvv, card_type.card_type, payment_system_type.payment_system_type, currency.currency " +
            "FROM paypanda_system.card  " +
            "JOIN paypanda_system.card_type ON card.card_type_id = card_type.card_type_id " +
            "JOIN paypanda_system.payment_system_type ON card.payment_system_type_id = payment_system_type.payment_system_type_id " +
            "JOIN paypanda_system.currency ON card.code_currency = currency.code_currency " +
            "WHERE card.card_id= ?";


    @Override
    public List<Card> takeAll() throws DAOException {
        return null;
    }

    /**
     * Represents the way of getting card as an instance of
     * {@code Card} through {@code id} value.
     *
     * @param id is card's identification number in database.
     * @return the only instance of {@code Card} that defined just by using
     * {@code id}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public Card takeById(Long id) throws DAOException {
        Card card = new Card();
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_CARD_BY_ID);
            preparedStatement.setLong(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                card.setNumber(resultSet.getLong(CARD_NUMBER));
                card.setId(resultSet.getLong(CARD_ID));
                card.setFirstNameHolder(resultSet.getString(FIRST_NAME_HOLDER));
                card.setLastNameHolder(resultSet.getString(LAST_NAME_HOLDER));
                CardType cardType = new CardType();
                cardType.setType(resultSet.getString(CARD_TYPE));
                card.setCardType(cardType);
                PaymentSystemType paymentSystemType = new PaymentSystemType();
                paymentSystemType.setType(resultSet.getString(PAYMENT_SYSTEM_TYPE));
                card.setPaymentSystemType(paymentSystemType);
                card.setCvv(resultSet.getInt(CVV));
                User user = new User();
                user.setId(resultSet.getLong(USER_ID));
                card.setUser(user);
            }
            return card;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during card taking", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }
    }

    /**
     * Adds definite card to database.
     *
     * @param card is an instance of {@code Card} class that
     *             has to be placed into table.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public void add(Card card) throws DAOException {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_ADD_CARD);

            Long cardNumber = card.getNumber();
            preparedStatement.setLong(1, cardNumber);

            Long userId = card.getUser().getId();
            preparedStatement.setLong(2, userId);

            Long cardTypeId = card.getCardType().getId();
            preparedStatement.setLong(3, cardTypeId);

            Long pstID = card.getPaymentSystemType().getId();
            preparedStatement.setLong(4, pstID);

            int cvv = card.getCvv();
            preparedStatement.setInt(5, cvv);

            String name = card.getFirstNameHolder();
            preparedStatement.setString(6, name);

            String lastName = card.getLastNameHolder();
            preparedStatement.setString(7, lastName);

            Long currencyId = card.getCurrencyType().getId();
            preparedStatement.setLong(8, currencyId);

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during card adding", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }
    }

    @Override
    public void deleteById(Long id) throws DAOException {

    }

    @Override
    public void updateById(Long id, Card card) throws DAOException {

    }

    /**
     * Gives the list of card of definite type from database.
     *
     * @param userId is user's identification number in database.
     * @return the list of cards collected by {@code userId}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    public List<Card> takeAllByUserId(Long userId) throws DAOException {
        List<Card> cards = new ArrayList<>();
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_CARD_BY_USER_ID);
            preparedStatement.setLong(1, userId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Card card = new Card();
                long number = resultSet.getLong(CARD_NUMBER);
                card.setNumber(number);
                card.setFirstNameHolder(resultSet.getString(FIRST_NAME_HOLDER));
                card.setLastNameHolder(resultSet.getString(LAST_NAME_HOLDER));
                CardType cardType = new CardType();
                cardType.setType(resultSet.getString(CARD_TYPE));
                card.setCardType(cardType);
                PaymentSystemType paymentSystemType = new PaymentSystemType();
                paymentSystemType.setType(resultSet.getString(PAYMENT_SYSTEM_TYPE));
                card.setPaymentSystemType(paymentSystemType);
                CurrencyType currencyType = new CurrencyType();
                currencyType.setName(resultSet.getString(CURRENCY));
                card.setCurrencyType(currencyType);
                cards.add(card);
            }
            return cards;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during cards taking", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }
    }

    /**
     * Represents the way of getting card as an instance of
     * {@code Card} through {@code number} value.
     *
     * @param number is card's number.
     * @return the only instance of {@code Card} that defined just by using
     * {@code number}.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    public Card takeByNumber(long number) throws DAOException {
        Card card = new Card();
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_CARD_BY_NUMBER);
            preparedStatement.setLong(1, number);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                card.setId(resultSet.getLong(CARD_ID));
                card.setNumber(resultSet.getLong(CARD_NUMBER));
                card.setLastNameHolder(resultSet.getString(LAST_NAME_HOLDER));
                card.setFirstNameHolder(resultSet.getString(FIRST_NAME_HOLDER));
                CardType cardType = new CardType();
                cardType.setType(resultSet.getString(CARD_TYPE));
                card.setCardType(cardType);
                PaymentSystemType paymentSystemType = new PaymentSystemType();
                paymentSystemType.setType(resultSet.getString(PAYMENT_SYSTEM_TYPE));
                card.setPaymentSystemType(paymentSystemType);
                card.setCvv(resultSet.getInt(CVV));
                User user = new User();
                user.setId(resultSet.getLong(USER_ID));
                card.setUser(user);
            }
            return card;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during card taking", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }
    }
}
