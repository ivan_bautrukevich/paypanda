package by.bautruk.paybau.dao;

import by.bautruk.paybau.entity.Payment;
import by.bautruk.paybau.entity.PaymentAccount;
import by.bautruk.paybau.entity.PaymentType;
import by.bautruk.paybau.exception.DAOException;
import by.bautruk.paybau.pool.ConnectionPool;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ivan.bautruvkevich
 *         17.01.2016 12:59.
 *         The {@code PaymentDAO} class implements all the necessary methods
 *         which implements {@code IDAO} interface for manipulation
 *         with payments's data in database.
 */
public class PaymentDAO implements IDao<Payment> {

    private static final String PAYMENT_ACCOUNT_NUMBER = "payment_account_number";
    private static final String DATE = "date";
    private static final String EXTERNAL_ACCOUNT = "external_account";
    private static final String DESCRIPTION = "description";
    private static final String PAYMENT_TYPE = "payment_type";
    private static final String SUM = "sum";

    private static final String SQL_ADD_PAYMENT = "INSERT INTO paypanda_system.payment(payment_account_id, date, external_account, sum, " +
            "description, payment_type_id) VALUES (?, ?, ?, ?, ?, ?)";
    private static final String SQL_FIND_PAYMENTS_BY_USER_ID = "SELECT payment_account.payment_account_number, payment.date, " +
            "payment.external_account, payment.sum, payment.description, payment_type.payment_type " +
            "FROM paypanda_system.payment " +
            "JOIN paypanda_system.payment_type ON payment.payment_type_id = payment_type.payment_type_id " +
            "JOIN paypanda_system.payment_account ON payment.payment_account_id = payment_account.payment_account_id " +
            "JOIN paypanda_system.card ON payment_account.card_id = card.card_id " +
            "JOIN paypanda_system.user ON card.user_id = user.user_id " +
            "WHERE user.user_id =? ORDER BY payment.date DESC";

    private static final String SQL_FIND_PAYMENTS_BY_DATE = "SELECT payment_account.payment_account_number, payment.date, " +
            "payment.external_account, payment.sum, payment.description, payment_type.payment_type " +
            "FROM paypanda_system.payment " +
            "JOIN paypanda_system.payment_type ON payment.payment_type_id = payment_type.payment_type_id " +
            "JOIN paypanda_system.payment_account ON payment.payment_account_id = payment_account.payment_account_id " +
            "JOIN paypanda_system.card ON payment_account.card_id = card.card_id " +
            "JOIN paypanda_system.user ON card.user_id = user.user_id " +
            "WHERE payment.date =?";

    @Override
    public List takeAll() throws DAOException {
        return null;
    }

    @Override
    public Payment takeById(Long id) throws DAOException {
        return null;
    }

    /**
     * Adds definite payment to database.
     *
     * @param payment is an instance of {@code Payment} class that
     *                has to be placed into table.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    @Override
    public void add(Payment payment) throws DAOException {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_ADD_PAYMENT);
            Long cardId = payment.getPaymentAccount().getId();
            preparedStatement.setLong(1, cardId);

            Date date = (Date) payment.getDate();
            preparedStatement.setDate(2, date);

            Long externalPaymentAccount = payment.getExternalPaymentAccount();
            preparedStatement.setLong(3, externalPaymentAccount);

            Float sum = payment.getSum();
            preparedStatement.setFloat(4, sum);

            String description = payment.getDescription();
            preparedStatement.setString(5, description);

            Long pdId = payment.getPaymentType().getId();
            preparedStatement.setLong(6, pdId);

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during payment adding", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }
    }

    @Override
    public void deleteById(Long id) throws DAOException {

    }

    @Override
    public void updateById(Long id, Payment payment) throws DAOException {

    }

    /**
     * Gives the list of payments of definite user ID from database.
     *
     * @param userId is a user's ID.
     * @return the list of payment.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    public List<Payment> takeAllByUserId(Long userId) throws DAOException {
        List<Payment> payments = new ArrayList<>();
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_PAYMENTS_BY_USER_ID);
            preparedStatement.setLong(1, userId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Payment payment = new Payment();
                PaymentAccount paymentAccount = new PaymentAccount();
                long number = resultSet.getLong(PAYMENT_ACCOUNT_NUMBER);
                paymentAccount.setNumber(number);
                payment.setPaymentAccount(paymentAccount);
                Date date = resultSet.getDate(DATE);
                payment.setDate(date);
                long eps = resultSet.getLong(EXTERNAL_ACCOUNT);
                payment.setExternalPaymentAccount(eps);
                payment.setSum(resultSet.getFloat(SUM));
                payment.setDescription(resultSet.getString(DESCRIPTION));
                PaymentType paymentType = new PaymentType();
                paymentType.setType(resultSet.getString(PAYMENT_TYPE));
                payment.setPaymentType(paymentType);
                payments.add(payment);
            }
            return payments;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during payments taking", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }
    }

    /**
     * Gives the list of payments of definite date from database.
     *
     * @param date is a selected day of payments.
     * @return the list of payment.
     * @throws DAOException if unable to get connection to database or
     *                      if there are any troubles with query execution.
     */

    public List<Payment> takeAllByDate(Date date) throws DAOException {
        List<Payment> payments = new ArrayList<>();
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        try {
            connection = pool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_PAYMENTS_BY_DATE);
            preparedStatement.setDate(1, date);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Payment payment = new Payment();
                PaymentAccount paymentAccount = new PaymentAccount();
                long number = resultSet.getLong(PAYMENT_ACCOUNT_NUMBER);
                paymentAccount.setNumber(number);
                payment.setPaymentAccount(paymentAccount);
                payment.setDate(date);
                long eps = resultSet.getLong(EXTERNAL_ACCOUNT);
                payment.setExternalPaymentAccount(eps);
                payment.setSum(resultSet.getFloat(SUM));
                payment.setDescription(resultSet.getString(DESCRIPTION));
                PaymentType paymentType = new PaymentType();
                paymentType.setType(resultSet.getString(PAYMENT_TYPE));
                payment.setPaymentType(paymentType);
                payments.add(payment);
            }
            return payments;
        } catch (SQLException e) {
            throw new DAOException("Invalid database operation occurred during payments taking", e);
        } finally {
            close(preparedStatement);
            pool.returnConnection(connection);
        }
    }
}
