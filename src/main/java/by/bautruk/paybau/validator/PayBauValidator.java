package by.bautruk.paybau.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author ivan.bautruvkevich
 *         16.01.2016 12:36.
 *         The {@code PayBauValidator} class validate data.
 */
public class PayBauValidator {

    private static final String LOGIN_REGEXP = "^[A-z0-9]{1,20}$";
    private static final String PASSWORD_REGEXP = "(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}";
    private static final String FIRST_NAME_REGEXP = "^[A-Z-А-Я-Ё]{1}[a-z-а-яё]{1,20}$";
    private static final String LAST_NAME_REGEXP = "^[A-Z-А-Я-Ё]{1}[a-z-а-яё]{1,20}$";
    private static final String EMAIL_REGEXP = "^([A-z0-9]+@[A-z]+\\.[A-z]+)$";
    private static final String PAYMENT_ACCOUNT_REGEXP = "^\\d{13}$";
    private static final String SUM_REGEXP = "^((\\d){1,10}[.]*){1}\\d{0,2}$";
    private static final String DESCRIPTION_REGEXP = ".{1,20}";
    private static final String CVV_REGEXP = "^[0-9]{3}$";
    private static final String CARD_NUMBER_REGEXP = "^\\d{16}$";

    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";
    private static final String EMAIL = "email";
    private static final String PAYMENT_ACCOUNT = "account";
    private static final String SUM = "sum";
    private static final String DESCRIPTION = "description";
    private static final String CVV = "cvv";
    private static final String CARD_NUMBER = "cardNumber";
    private static final int CHECK_COUNT = 1;


    /**
     * Defines the way of data validation.
     *
     * @param field is the string value, which use for find
     *              right regex expression.
     * @param data  is the string value, which have to be validated.
     * @return if the value of {@param data} is valid, returns
     * {@code true}, otherwise returns {@code false}.
     */

    public boolean checkData(String field, String data) {
        int count = 0;
        String regexpPattern = null;
        switch (field) {
            case LOGIN:
                regexpPattern = LOGIN_REGEXP;
                break;
            case PASSWORD:
                regexpPattern = PASSWORD_REGEXP;
                break;
            case FIRST_NAME:
                regexpPattern = FIRST_NAME_REGEXP;
                break;
            case LAST_NAME:
                regexpPattern = LAST_NAME_REGEXP;
                break;
            case EMAIL:
                regexpPattern = EMAIL_REGEXP;
                break;
            case PAYMENT_ACCOUNT:
                regexpPattern = PAYMENT_ACCOUNT_REGEXP;
                break;
            case SUM:
                regexpPattern = SUM_REGEXP;
                break;
            case DESCRIPTION:
                regexpPattern = DESCRIPTION_REGEXP;
                break;
            case CVV:
                regexpPattern = CVV_REGEXP;
                break;
            case CARD_NUMBER:
                regexpPattern = CARD_NUMBER_REGEXP;
                break;
        }
        Pattern pattern = Pattern.compile(regexpPattern);
        Matcher matcher = pattern.matcher(data);
        while (matcher.find()) {
            count++;
        }
        return count == CHECK_COUNT;
    }
}
