package by.bautruk.paybau.entity;

/**
 * @author ivan.bautruvkevich
 *         06.01.2016 13:20.
 *         The {@code PaymentAccountStatus} class is an entity-class which includes all
 *         the necessary getters and setters for its fields and also overrides
 *         {@code equals()}, {@code hashCode()} and {@code toString()}.
 */
public class PaymentAccountStatus extends Entity{
    private String status;

    public PaymentAccountStatus() {
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PaymentAccountStatus)) return false;

        PaymentAccountStatus that = (PaymentAccountStatus) o;

        return status != null ? status.equals(that.status) : that.status == null;

    }

    @Override
    public int hashCode() {
        return status != null ? status.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "PaymentAccountStatus{" +
                "status='" + status + '\'' +
                '}';
    }
}
