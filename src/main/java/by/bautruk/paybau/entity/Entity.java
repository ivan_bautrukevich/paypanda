package by.bautruk.paybau.entity;

import java.io.Serializable;

/**
 * @author ivan.bautruvkevich
 *         06.01.2016 13:09.
 *         The {@code Entity} class is an entity-class which includes all
 *         the necessary getters and setters for its fields and also overrides
 *         {@code equals()}, {@code hashCode()} and {@code toString()}.
 */
public class Entity implements Serializable, Cloneable { //// FIXME: 15.02.2016 check implements in extends classes
    private Long id;

    public Entity() {
    }

    public Entity(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
