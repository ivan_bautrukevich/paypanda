package by.bautruk.paybau.entity;

/**
 * @author ivan.bautruvkevich
 *         06.01.2016 13:18.
 *         The {@code PaymentAccount} class is an entity-class which includes all
 *         the necessary getters and setters for its fields and also overrides
 *         {@code equals()}, {@code hashCode()} and {@code toString()}.
 */
public class PaymentAccount extends Entity {
    private long number;
    private Card card;
    private float balance;
    private PaymentAccountStatus paymentAccountStatus;


    public PaymentAccount() {
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public PaymentAccountStatus getPaymentAccountStatus() {
        return paymentAccountStatus;
    }

    public void setPaymentAccountStatus(PaymentAccountStatus paymentAccountStatus) {
        this.paymentAccountStatus = paymentAccountStatus;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PaymentAccount)) return false;

        PaymentAccount that = (PaymentAccount) o;

        if (number != that.number) return false;
        if (Float.compare(that.balance, balance) != 0) return false;
        if (card != null ? !card.equals(that.card) : that.card != null) return false;
        return paymentAccountStatus != null ? paymentAccountStatus.equals(that.paymentAccountStatus) : that.paymentAccountStatus == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (number ^ (number >>> 32));
        result = 31 * result + (card != null ? card.hashCode() : 0);
        result = 31 * result + (balance != +0.0f ? Float.floatToIntBits(balance) : 0);
        result = 31 * result + (paymentAccountStatus != null ? paymentAccountStatus.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PaymentAccount{" +
                "number=" + number +
                ", card=" + card +
                ", balance=" + balance +
                ", paymentAccountStatus=" + paymentAccountStatus +
                '}';
    }
}
