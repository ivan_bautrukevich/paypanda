package by.bautruk.paybau.entity;

/**
 * @author ivan.bautruvkevich
 *         06.01.2016 13:17.
 *         The {@code CurrencyType} class is an entity-class which includes all
 *         the necessary getters and setters for its fields and also overrides
 *         {@code equals()}, {@code hashCode()} and {@code toString()}.
 */
public class CurrencyType extends Entity {
    private int code;
    private String name;

    public CurrencyType() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CurrencyType)) return false;

        CurrencyType that = (CurrencyType) o;

        if (code != that.code) return false;
        return name != null ? name.equals(that.name) : that.name == null;

    }

    @Override
    public int hashCode() {
        int result = code;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CurrencyType{" +
                "code=" + code +
                ", name='" + name + '\'' +
                '}';
    }
}
