package by.bautruk.paybau.entity;

/**
 * @author ivan.bautruvkevich
 *         06.01.2016 13:22.
 *         The {@code PaymentSystemType} class is an entity-class which includes all
 *         the necessary getters and setters for its fields and also overrides
 *         {@code equals()}, {@code hashCode()} and {@code toString()}.
 */
public class PaymentSystemType extends Entity {
    private String type;

    public PaymentSystemType() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PaymentSystemType)) return false;

        PaymentSystemType that = (PaymentSystemType) o;

        return type != null ? type.equals(that.type) : that.type == null;

    }

    @Override
    public int hashCode() {
        return type != null ? type.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "PaymentSystemType{" +
                "type='" + type + '\'' +
                '}';
    }
}
