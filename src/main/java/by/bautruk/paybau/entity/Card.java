package by.bautruk.paybau.entity;

/**
 * @author ivan.bautruvkevich
 *         06.01.2016 13:23.
 *         The {@code Card} class is an entity-class which includes all
 *         the necessary getters and setters for its fields and also overrides
 *         {@code equals()}, {@code hashCode()} and {@code toString()}.
 */
public class Card extends Entity {
    private long number;
    private User user;
    private CardType cardType;
    private PaymentSystemType paymentSystemType;
    private int cvv;
    private String firstNameHolder;
    private String lastNameHolder;
    private CurrencyType currencyType;

    public Card() {
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public CardType getCardType() {
        return cardType;
    }

    public void setCardType(CardType cardType) {
        this.cardType = cardType;
    }

    public PaymentSystemType getPaymentSystemType() {
        return paymentSystemType;
    }

    public void setPaymentSystemType(PaymentSystemType paymentSystemType) {
        this.paymentSystemType = paymentSystemType;
    }

    public int getCvv() {
        return cvv;
    }

    public void setCvv(int cvv) {
        this.cvv = cvv;
    }

    public String getFirstNameHolder() {
        return firstNameHolder;
    }

    public void setFirstNameHolder(String firstNameHolder) {
        this.firstNameHolder = firstNameHolder;
    }

    public String getLastNameHolder() {
        return lastNameHolder;
    }

    public void setLastNameHolder(String lastNameHolder) {
        this.lastNameHolder = lastNameHolder;
    }

    public CurrencyType getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(CurrencyType currencyType) {
        this.currencyType = currencyType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Card)) return false;

        Card card = (Card) o;

        if (number != card.number) return false;
        if (cvv != card.cvv) return false;
        if (user != null ? !user.equals(card.user) : card.user != null) return false;
        if (cardType != null ? !cardType.equals(card.cardType) : card.cardType != null) return false;
        if (paymentSystemType != null ? !paymentSystemType.equals(card.paymentSystemType) : card.paymentSystemType != null)
            return false;
        if (firstNameHolder != null ? !firstNameHolder.equals(card.firstNameHolder) : card.firstNameHolder != null)
            return false;
        if (lastNameHolder != null ? !lastNameHolder.equals(card.lastNameHolder) : card.lastNameHolder != null)
            return false;
        return currencyType != null ? currencyType.equals(card.currencyType) : card.currencyType == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (number ^ (number >>> 32));
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (cardType != null ? cardType.hashCode() : 0);
        result = 31 * result + (paymentSystemType != null ? paymentSystemType.hashCode() : 0);
        result = 31 * result + cvv;
        result = 31 * result + (firstNameHolder != null ? firstNameHolder.hashCode() : 0);
        result = 31 * result + (lastNameHolder != null ? lastNameHolder.hashCode() : 0);
        result = 31 * result + (currencyType != null ? currencyType.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Card{" +
                "number=" + number +
                ", user=" + user +
                ", cardType=" + cardType +
                ", paymentSystemType=" + paymentSystemType +
                ", cvv=" + cvv +
                ", firstNameHolder='" + firstNameHolder + '\'' +
                ", lastNameHolder='" + lastNameHolder + '\'' +
                ", currencyType=" + currencyType +
                '}';
    }
}
