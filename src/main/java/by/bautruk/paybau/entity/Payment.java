package by.bautruk.paybau.entity;

import java.util.Date;

/**
 * @author ivan.bautruvkevich
 *         06.01.2016 13:26.
 *         The {@code Payment} class is an entity-class which includes all
 *         the necessary getters and setters for its fields and also overrides
 *         {@code equals()}, {@code hashCode()} and {@code toString()}.
 */
public class Payment extends Entity {
    private PaymentAccount paymentAccount;
    private Date date;
    private long externalPaymentAccount;
    private float sum;
    private String description;
    private PaymentType paymentType;

    public Payment() {
    }

    public PaymentAccount getPaymentAccount() {
        return paymentAccount;
    }

    public void setPaymentAccount(PaymentAccount paymentAccount) {
        this.paymentAccount = paymentAccount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public long getExternalPaymentAccount() {
        return externalPaymentAccount;
    }

    public void setExternalPaymentAccount(long externalPaymentAccount) {
        this.externalPaymentAccount = externalPaymentAccount;
    }

    public float getSum() {
        return sum;
    }

    public void setSum(float sum) {
        this.sum = sum;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Payment)) return false;

        Payment payment = (Payment) o;

        if (externalPaymentAccount != payment.externalPaymentAccount) return false;
        if (Float.compare(payment.sum, sum) != 0) return false;
        if (paymentAccount != null ? !paymentAccount.equals(payment.paymentAccount) : payment.paymentAccount != null)
            return false;
        if (date != null ? !date.equals(payment.date) : payment.date != null) return false;
        if (description != null ? !description.equals(payment.description) : payment.description != null) return false;
        return paymentType != null ? paymentType.equals(payment.paymentType) : payment.paymentType == null;

    }

    @Override
    public int hashCode() {
        int result = paymentAccount != null ? paymentAccount.hashCode() : 0;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (int) (externalPaymentAccount ^ (externalPaymentAccount >>> 32));
        result = 31 * result + (sum != +0.0f ? Float.floatToIntBits(sum) : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (paymentType != null ? paymentType.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "paymentAccount=" + paymentAccount +
                ", date=" + date +
                ", externalPaymentAccount=" + externalPaymentAccount +
                ", sum=" + sum +
                ", description='" + description + '\'' +
                ", paymentType=" + paymentType +
                '}';
    }
}
