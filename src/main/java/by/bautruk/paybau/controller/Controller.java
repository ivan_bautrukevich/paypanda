package by.bautruk.paybau.controller;

import by.bautruk.paybau.config.ConfigManager;
import by.bautruk.paybau.exception.ApplicationException;
import by.bautruk.paybau.navigation.ActionCommand;
import by.bautruk.paybau.navigation.ActionFactory;
import by.bautruk.paybau.pool.ConnectionPool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author ivan.bautruvkevich
 *         08.01.2016 16:57.
 */
@WebServlet("/controller")
public class Controller extends HttpServlet {

    final static Logger LOG = LogManager.getLogger(Controller.class);

    private static final String PAGE_ERROR = "page.error";
    private static final String MESSAGE = "message";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ActionFactory client = new ActionFactory();
        ActionCommand command;
        String page;
        try {
            command = client.defineCommand(req);
            page = command.execute(req);
        } catch (ApplicationException e) {
            LOG.error(e);
            req.getSession().setAttribute(MESSAGE, e.getMessage());
            page = ConfigManager.getProperty(PAGE_ERROR);
        }
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
        dispatcher.forward(req, resp);
    }

    @Override
    public void destroy() {
        super.destroy();
        ConnectionPool.getInstance().cleanUp();
    }
}
