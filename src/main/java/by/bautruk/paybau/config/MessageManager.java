package by.bautruk.paybau.config;

import java.util.ResourceBundle;

/**
 * @author ivan.bautruvkevich
 *         10.01.2016 14:32.
 *         The {@code MessageManager} class represents the way select
 *         massenge on right language from conformity file.
 */
public class MessageManager {

    private static final String LINK_RU = "messages_ru_RU";
    private static final String LINK_EN = "messages_en_US";
    private static final String RU = "ru_RU";
    private static final String EN = "en_US";

    private MessageManager() {
    }

    public static String getProperty(String key, String locale) {
        ResourceBundle resourceBundle;
        switch (locale) {
            case RU:
                resourceBundle = ResourceBundle.getBundle(LINK_RU);
                break;
            case EN:
                resourceBundle = ResourceBundle.getBundle(LINK_EN);
                break;
            default:
                resourceBundle = ResourceBundle.getBundle(LINK_EN);
                break;
        }
        return resourceBundle.getString(key);
    }
}
