package by.bautruk.paybau.config;

import java.util.ResourceBundle;

/**
 * @author ivan.bautruvkevich
 *         10.01.2016 13:03.
 *         The {@code ConfigManager} class represents the way to use
 *         notes from configuration property file.
 */
public class ConfigManager {

    private static final String LINK = "config";

    private final static ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(LINK);

    private ConfigManager() {
    }

    public static String getProperty(String key) {
        return RESOURCE_BUNDLE.getString(key);
    }
}
