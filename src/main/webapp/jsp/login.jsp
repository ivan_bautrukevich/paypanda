<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 10.01.2016
  Time: 13:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="pagecontent" prefix="page.login.">
    <head>
        <link href="/image/favicon.ico" rel="shortcut icon" type="image/x-icon">
        <title><fmt:message key="title"/></title>
        <link rel="stylesheet" type="text/css" href="../css/style.css">
    </head>
    <body>
    <div class="login">
        <div class="headerLogin">
            <div class="imagePandaLogin">
                <img src="/image/pandaLogin.JPG" height="123" , width="100">
            </div>
            <div class="logo">
                <p>PayPanda</p>
            </div>
            <div class="headerTableLogin">
                <%@ include file="include/language.jsp" %>
            </div>
        </div>
        <div class="infoLogin">
            <h3 align="center"><fmt:message key="welcome"/></h3>
            <form name="loginForm" method="POST" action="controller">
                <input type="hidden" name="command" value="login"/>
                <table align="center">
                    <tr>
                        <td>
                            <fmt:message key="login"/>
                        </td>
                        <td>
                            <input required type="text" name="login" value=""/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fmt:message key="password"/>
                        </td>
                        <td>
                            <input required type="password" name="password" value=""/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <input class="myButton" type="submit" value="<fmt:message key="log_in"/>"/>
                        </td>
                    </tr>
                </table>
                <p align="center">${errorLoginPassMessage}</p>
            </form>
            <form name="signUpForm" method="POST" action="controller">
                <input type="hidden" name="command" value="signup"/>
                <table align="center">
                    <tr>
                        <td>
                            <button class="myButton" type="submit"><fmt:message key="signup"/></button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
    </body>
    </html>
</fmt:bundle>