<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 03.02.2016
  Time: 10:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="pagecontent" prefix="page.takeUsers.">
    <head>
        <link href="/image/favicon.ico" rel="shortcut icon" type="image/x-icon">
        <title><fmt:message key="title"/></title>
    </head>
    <body>
    <div class="central">
        <div class="head">
            <%@ include file="../include/header.jsp" %>
        </div>
        <div class="menuUser">
            <%@ include file="../include/menuAdmin.jsp" %>
        </div>
        <div class="main">
            <form type="unblockedForm" method="POST" action="controller">
                <input type="hidden" name="command" value="delete_users"/>
                <table class="tablePanda">
                    <tr>
                        <th><fmt:message key="firstName"/></th>
                        <th><fmt:message key="lastName"/></th>
                        <th><fmt:message key="email"/></th>
                        <th><fmt:message key="role"/></th>
                        <th><fmt:message key="delete"/></th>
                    </tr>
                    <c:forEach var="user" items="${users}">
                        <tr>
                            <td>${user.firstName}</td>
                            <td>${user.lastName}</td>
                            <td>${user.email}</td>
                            <td>${user.role.role}</td>
                            <td><input type="checkbox" name="deleteUsers" value="${user.id}"></td>
                        </tr>
                    </c:forEach>
                    <tr>
                        <td colspan="6" align="center">
                            <input align="center" class="secondButton" type="submit"
                                   value="<fmt:message key="delete"/>"/>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        <%@ include file="../include/footer.jsp" %>
    </div>
    </body>
    </html>
</fmt:bundle>
