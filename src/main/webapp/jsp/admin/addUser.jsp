<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 02.02.2016
  Time: 19:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="pagecontent" prefix="page.signUp.">
    <head>
        <link href="/image/favicon.ico" rel="shortcut icon" type="image/x-icon">
        <title><fmt:message key="title"/></title>
    </head>
    <body>
    <div class="central">
        <div class="head">
            <%@ include file="../include/header.jsp" %>
        </div>
        <div class="menuUser">
            <%@ include file="../include/menuAdmin.jsp" %>
        </div>
        <div class="main">
            <h3 align="center"><fmt:message key="ad"/></h3>
            <form type="loginForm" method="POST" action="controller">
                <input type="hidden" name="command" value="registration"/>
                <table align="center">
                    <tr>
                        <td>
                            <fmt:message key="firstName"/>
                        </td>
                        <td>
                            <input required type="text" name="firstName" value="" pattern="^[A-Z-А-Я-Ё]{1}[a-z-а-яё]{1,20}$"
                                   title="<fmt:message key="firstName.regex"/>"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fmt:message key="lastName"/>
                        </td>
                        <td>
                            <input required type="text" name="lastName" value="" pattern="^[A-Z-А-Я-Ё]{1}[a-z-а-яё]{1,20}$"
                                   title="<fmt:message key="firstName.regex"/>"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fmt:message key="email"/>
                        </td>
                        <td>
                            <input required type="text" name="email" value="" pattern="^([A-z0-9]+@[A-z]+\.[A-z]+)$"
                                   title="<fmt:message key="email.regex"/>"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fmt:message key="login"/>
                        </td>
                        <td>
                            <input required type="text" name="login" value="" pattern="^[A-z0-9]{1,20}$"
                                   title="<fmt:message key="login.regex"/>"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fmt:message key="password"/>
                        </td>
                        <td>
                            <input required type="password" name="password" value=""
                                   pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                                   title="<fmt:message key="password.regex"/>"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fmt:message key="role"/>
                        </td>
                        <td>
                            <select name="selectRole">
                                <c:forEach var="role" items="${roles}">
                                    <option selected="select">${role.role}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <input class="secondButton" type="submit" value="<fmt:message key="submit"/>"/>
                        </td>
                    </tr>
                </table>
                    ${message}
            </form>
        </div>
        <%@ include file="../include/footer.jsp" %>
    </div>
    </body>
    </html>
</fmt:bundle>
