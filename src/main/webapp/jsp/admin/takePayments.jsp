<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 03.02.2016
  Time: 10:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="pagecontent" prefix="page.takePayments.">
    <head>
        <link href="/image/favicon.ico" rel="shortcut icon" type="image/x-icon">
        <title><fmt:message key="title"/></title>
    </head>
    <body>
    <div class="central">
        <div class="head">
            <%@ include file="../include/header.jsp" %>
        </div>
        <div class="menuUser">
            <%@ include file="../include/menuAdmin.jsp" %>
        </div>
        <div class="main">
            <form>
                <input type="date" name="calendar">
                <input type="hidden" name="command" value="day_payments"/>
                <input class="secondButton" type="submit" value="<fmt:message key="submit"/>"/>
            </form>
            <c:choose>
                <c:when test="${payments.size() > 0}">
                    <table class="tablePanda">
                        <tr>
                            <th><fmt:message key="account"/></th>
                            <th><fmt:message key="date"/></th>
                            <th><fmt:message key="externalAccount"/></th>
                            <th><fmt:message key="sum"/></th>
                            <th><fmt:message key="description"/></th>
                            <th><fmt:message key="paymentType"/></th>
                        </tr>
                        <c:forEach var="payment" items="${payments}">
                            <tr>
                                <td>${payment.paymentAccount.number}</td>
                                <td>${payment.date}</td>
                                <td>${payment.externalPaymentAccount}</td>
                                <td>${payment.sum}</td>
                                <td>${payment.description}</td>
                                <td>${payment.paymentType}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </c:when>
                <c:when test="${payments == null}">
                    <fmt:message key="message.empty"/>
                </c:when>
                <c:when test="${payments.size() == 0}">
                    ${day} <fmt:message key="message.admin"/>
                </c:when>
            </c:choose>

        </div>
        <%@ include file="../include/footer.jsp" %>
    </div>
    </body>
    </html>
</fmt:bundle>
