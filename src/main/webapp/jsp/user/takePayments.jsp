<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 17.01.2016
  Time: 12:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="pagecontent" prefix="page.takePayments.">
    <head>
        <link href="/image/favicon.ico" rel="shortcut icon" type="image/x-icon">
        <title><fmt:message key="title"/></title>
    </head>
    <body>
    <div class="central">
        <div class="head">
            <%@ include file="../include/header.jsp" %>
        </div>
        <div class="menuUser">
            <%@ include file="../include/menuUser.jsp" %>
        </div>
        <div class="main">
            <c:choose>
                <c:when test="${payments.size() > 0}">
                    <table class="tablePanda">
                        <tr>
                            <th><fmt:message key="account"/></th>
                            <th><fmt:message key="date"/></th>
                            <th><fmt:message key="externalAccount"/></th>
                            <th><fmt:message key="sum"/></th>
                            <th><fmt:message key="description"/></th>
                            <th><fmt:message key="paymentType"/></th>
                        </tr>
                        <c:forEach var="payment" items="${payments}">
                            <tr>
                                <td>${payment.paymentAccount.number}</td>
                                <td>${payment.date}</td>
                                <td>${payment.externalPaymentAccount}</td>
                                <td>${payment.sum}</td>
                                <td>${payment.description}</td>
                                <td>${payment.paymentType}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </c:when>
                <c:otherwise>
                    <fmt:message key="message"/>
                </c:otherwise>
            </c:choose>
        </div>
        <%@ include file="../include/footer.jsp" %>
    </div>
    </body>
    </html>
</fmt:bundle>
