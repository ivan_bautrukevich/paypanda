<%--
  Created by IntelliJ IDEA.
  User: Ivan
  Date: 22.01.2016
  Time: 12:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<fmt:setLocale value="${locale}"/>
<fmt:bundle basename="pagecontent" prefix="page.addCard.">
    <head>
        <link href="/image/favicon.ico" rel="shortcut icon" type="image/x-icon">
        <title><fmt:message key="title"/></title>
    </head>
    <body>
    <div class="central">
        <div class="head">
            <%@ include file="../include/header.jsp" %>
        </div>
        <div class="menuUser">
            <%@ include file="../include/menuUser.jsp" %>
        </div>
        <div class="main">
            <fmt:message key="ad"/>
            <form name="loginForm" method="POST" action="controller">
                <input type="hidden" name="command" value="add_card"/>
                <table align="center">
                    <tr>
                        <td>
                            <fmt:message key="cardNumber"/>
                        </td>
                        <td>
                            <input required type="text" name="cardNumber" value="" pattern="^\d{16}$"
                                   title="<fmt:message key="cardNumber.regex"/>"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fmt:message key="firstName"/>
                        </td>
                        <td>
                            <input type="text" name="firstName" value="" pattern="^[A-Z]{1}[a-z]{1,20}$"
                                   title="<fmt:message key="firstName.regex"/>"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fmt:message key="lastName"/>
                        </td>
                        <td>
                            <input type="text" name="lastName" value="" pattern="^[A-Z]{1}[a-z]{1,20}$"
                                   title="<fmt:message key="lastName.regex"/>"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fmt:message key="selectCardType"/>
                        </td>
                        <td>
                            <select name="selectCardType">
                                <c:forEach var="type" items="${cardTypes}">
                                    <option selected="select">${type.type}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fmt:message key="selectPaymentSystemType"/>
                        </td>
                        <td>
                            <select name="selectPaymentSystemType">
                                <c:forEach var="systemType" items="${paymentSystemTypes}">
                                    <option selected="select">${systemType.type}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fmt:message key="selectCurrency"/>
                        </td>
                        <td>
                            <select name="selectCurrency">
                                <c:forEach var="currency" items="${currencies}">
                                    <option selected="select">${currency.name}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fmt:message key="cvv"/>
                        </td>
                        <td>
                            <input type="text" name="cvv" value="" pattern="^[0-9]{3}?"
                                   title="<fmt:message key="cvv.regex"/>"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <input class="secondButton" type="submit" value="<fmt:message key="submit"/>"/>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        <%@ include file="../include/footer.jsp" %>
    </div>
    </body>
    </html>
</fmt:bundle>
